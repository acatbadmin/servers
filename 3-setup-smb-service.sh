#!/bin/bash

# ==============================================================================
# - This script should be used to create Shares and Share permissions
# - It assumes a standard Wasta 20.04 system as a starting point.
# - The configured shares are:
#   - formations
#   - wasta
#   - windows
#
# The general script outline is:
#	- Ensure dependencies are met
#	- Allow Samba in system firewall (esp. Ubuntu derivatives)
#	- Create (or modify) non-standard groups
#	- Create (or modify) users

#	- Create (or modify) folders
#	- Create (or modify) smb.conf to match
#
# ==============================================================================

# ------------------------------------------------------------------------------
# Global variables.
# ------------------------------------------------------------------------------
SHARE_BASE=/var/share
SUBNET="172.16.1.0/24"
ADMIN_USER="samba-admin"

# ------------------------------------------------------------------------------
# Check to ensure running as root.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}" "${@}"
    exit $?
fi

# echo "Attention ! Ce script installera samba et le configurera."
# read -p "Tappez [Enter] pour continuer ou Ctrl+C pour annuler..."


# ------------------------------------------------------------------------------
# Check that dependencies are installed.
# ------------------------------------------------------------------------------
if [[ ! $(which pdbedit) ]]; then
	apt-get --yes update
	apt-get --yes install nano samba samba-vfs-modules # system-config-samba?
fi


# ------------------------------------------------------------------------------
# Allow Samba through ufw (Ubuntu).
# ------------------------------------------------------------------------------
if [[ $(which ufw) ]]; then
	ufw allow from "${SUBNET}" to any app Samba
fi


# ------------------------------------------------------------------------------
# Define Functions.
# ------------------------------------------------------------------------------
newFolder() {
    folder="${1}"
    owner="${2}"
    group="${3}"
    mode="${4}"
	# echo
	echo "Creating folder: $folder."
	mkdir -p $SHARE_BASE/$folder
	chown $owner:$group $SHARE_BASE/$folder
	chmod $mode $SHARE_BASE/$folder
	# echo "- Dossier confirmé :"
	# echo "    $folder, propriétaire $owner, groupe $group, permissions $mode"
}

newGroup() {
    gid="${1}"
    gname="${2}"
	# echo
	echo "Creating group: $gname."
	addgroup --gid $gid $gname
}

newUser() {
    # The following variables assumed to be set before calling this function:
	# $userid, $username, $passwd
    userid="${1}"
    username="${2}"
    passwd="${3}"
	# echo

	# Delete and create user (unix account): (delete to make sure uid correct)
	# true returned if UID not found so won't error
	echo "-- $username --"
    curr_uid=$(id -u $username || true)
    if [[ "$curr_uid" ]] && ! [[ $curr_uid == $userid ]]; then
        echo
		echo "Deleting user to update the user uid: $username"
		read -p "- current uid: $curr_uid; new uid: $userid"
        deluser $username
    fi

    # # adduser: don't create home directory, except for $ADMIN_USER
	# echo "Confirmation d'utilisateur du système..."
	# if [[ $username == $ADMIN_USER ]]; then
	# 	adduser --uid $userid $username --disabled-password --gecos "$username"
	# else
	# 	adduser --uid $userid $username --disabled-password --no-create-home --gecos "$username"
	# fi

    # adduser: Don't create home directory.
	echo "Ensuring system user."
	adduser --uid $userid $username --disabled-password --no-create-home --gecos "$username"

	# create user (smb account): (no need to delete); give default pwd if none set
	echo "Ensuring samba user."
    pwd_status=$(passwd --status $username | awk '{print $2}')
    if [[ $pwd_status == "NP" ]]; then
        echo "$username:$passwd" | chpasswd
    fi

    smb_exists=$(pdbedit -u $username || true)
    if [[ ! "$smb_exists" ]]; then
		echo "Adding samba user: $username"
        (echo $passwd; echo $passwd) | smbpasswd -a -s $username
    fi
}

smbBlock() {
    folder="${1}"
    comment="${2}"
    path="${3}"
    browseable="${4}"
    mode="${5}"
    read_only="${6}"
    guest="${7}"
	echo "
[$folder]
Comment = $comment
path = $path
browseable = $browseable
read only = "$read_only"
guest ok = $guest
create mask = 0$mode" >> /etc/samba/smb.conf
	echo "Added \"$folder\" to smb.conf."
}

# ------------------------------------------------------------------------------
# Create users & add to relevant non-user groups.
# ------------------------------------------------------------------------------
echo "Adding new groups and users..."

newGroup "2001" "$ADMIN_USER"
newUser "2001" "$ADMIN_USER" "Esaie53:7"
adduser "$ADMIN_USER" "$ADMIN_USER"

# ------------------------------------------------------------------------------
# Create first section of /etc/samba/smb.conf.
# ------------------------------------------------------------------------------
# Determine ethernet interface.
#eth=$(nmcli device status | grep ethernet | awk '{print $1}')
eth=$(ip -br addr | grep -Eow '^en[ps0-9]+' | head -n1)

echo "Adding new folders..."
echo
if [[ ! -e /etc/samba/smb.conf.orig ]]; then
    cp /etc/samba/smb.conf{,.orig}
    #echo "Backed up original smb.conf to smb.conf.orig and created new version."
    echo "Original smb.conf was backed up."
fi

# Write [global] section, which does not change, to top of smb.conf.
echo "[global]
	workgroup = ACATBA
	server string = %h server (Samba, Ubuntu)
	interfaces = $eth
	server role = standalone server
	security = USER
	map to guest = bad user
	obey pam restrictions = yes
	pam password change = yes
	passwd program = /usr/bin/passwd %u
	passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
	unix password sync = yes
	log file = /var/log/samba/log.%m
	max log size = 1000
	os level = 65
	domain master = no
	dns proxy = no
	usershare allow guests = yes
	panic action = /usr/share/samba/panic-action %d
	idmap config * : backend = tdb

	# Added preferences by Nate
    log level = 3
    hide dot files = yes
    hide files = /~\$*/
    passdb backend = tdbsam" > /etc/samba/smb.conf


# ------------------------------------------------------------------------------
# Create the root folder of the samba share.
# ------------------------------------------------------------------------------
folder="share"
mode="774"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Dossier de tous les utilisateurs"
path="$SHARE_BASE/$folder"
browseable="no"
read_only="no"
guest="no"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

# ------------------------------------------------------------------------------
# Create the shared subfolders.
# ------------------------------------------------------------------------------
folder="apps-etc"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Applications et ressources"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="formations"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Materiels de formation"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="guérison des traumatismes"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Ressources sur la guérison des traumatismes"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="linguistique"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Ressources sur la linguistique"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="transferts"
mode="666"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Pour transferer des fichiers au serveur"
path="$SHARE_BASE/$folder"
browseable="no"
read_only="no"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="windows"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Mises a jour pour Windows"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

# ------------------------------------------------------------------------------
# system-config-samba bug fix.
# ------------------------------------------------------------------------------
# /etc/libuser.conf needs to exist for system-config-samba to be able to launch
# when installing system-config-samba this file isn't created :-(
# touch /etc/libuser.conf

# ------------------------------------------------------------------------------
# Restart samba related services.
# ------------------------------------------------------------------------------
systemctl restart nmbd.service
systemctl restart smbd.service

exit 0
