#!/bin/bash

# ==============================================================================
# This script is used to install and configure squid-deb-proxy.
# ==============================================================================
SCRIPT="${0}"
PARENT=$(dirname "${0}")


# ------------------------------------------------------------------------------
# Ensure elevated privileges.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}"
    exit $?
fi

echo "Attention ! Ce script installera squid-deb-proxy et le configurera."
read -p "Tappez [Enter] pour continuer ou Ctrl+C pour annuler..."

# Install packages
echo "Vérification de l'installation de squid-deb-proxy."
if [[ ! $(dpkg-query --show squid-deb-proxy 2>/dev/null) ]]; then
    sudo apt-get update
    echo "Installation de squid-deb-proxy..."
    sudo apt-get --yes install squid-deb-proxy squid-deb-proxy-client
fi

# Configure allowed repos.
echo "Vérification de configuration de squid-deb-proxy..."
mirror_dir="/etc/squid-deb-proxy/mirror-dstdomain.acl.d"

# Allow sourceforge repo (libdvd-pkg).
libdvd_repos="${mirror_dir}/20-sourceforge-downloads"
echo "downloads.sourceforge.net" > "$libdvd_repos"

# Configure to allow Wasta repos.
wasta_repos="${mirror_dir}/20-wasta-repos"
# Erase the existing file with the first entry.
echo "ppa.launchpad.net" > "$wasta_repos"
echo "packages.sil.org" >> "$wasta_repos"
echo "repo.skype.com" >> "$wasta_repos" # Skype
echo "dl.google.com" >> "$wasta_repos" # Chrome

# Flathub
flathub_repos="${mirror_dir}/20-flathub-repos"
# Erase the existing file with the first entry.
echo "flathub.org" > "$flathub_repos"

# Configure to allow Unifi repos.
unifi_repos="${mirror_dir}/20-unifi-repos"
# Erase the existing file with the first entry.
echo "repo.mongodb.org" > "$unifi_repos"
echo "www.ui.com" >> "$unifi_repos"

# Configure to allow armbian repos.
armbian_repos="${mirror_dir}/20-armbian-repos"
# Erase the existing file with the first entry.
echo "apt.armbian.com" > "$armbian_repos"
echo "armbian.systemonachip.net" >> "$armbian_repos"
echo "minio.k-space.ee" >> "$armbian_repos"
echo "ports.ubuntu.com" >> "$armbian_repos"
echo "mirrors.netix.net" >> "$armbian_repos"

# Configure to allow insync repo.
insync_repos="${mirror_dir}/20-insync-repos"
# Erase the existing file with the first entry.
echo "apt.insync.io" > "$insync_repos"

# Restart squid-deb-proxy service.
echo "Redémarrage de squid-deb-proxy.service (30 sec)..."
systemctl restart squid-deb-proxy.service


# Stop and disable squid.service.
systemctl status squid.service --no-pager 2>&1 >/dev/null
squid_status=$?
if [[ $squid_status -eq 0 ]]; then
    # Kill squid service
    echo "Arrêt de squid.service (30 sec)..."
    systemctl disable squid.service
    systemctl stop squid.service
fi
