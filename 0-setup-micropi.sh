#!/bin/bash

# Set up MicroPi as a WiFi-based file server for ACATBA.
SYSNAME='MicroPi'
PIIMAGEZ="RPZWBB20211204.0.img.zip"
SSID="ACATBA-PARTAGES"
HOSTNAME='acatba-partages'

# Set global variables.
LANG=C
MNTHOME="/media/$USER"
PIBOOT="${MNTHOME}/boot"
PIROOTFS="${MNTHOME}/rootfs"
PIHOME="${PIROOTFS}/home/pi"
PISITE="${PIHOME}/www"
PICONF="${PIHOME}/conf"

STORAGE="${MNTHOME}/Storage"
SOURCE_DIRS="audio images software text video"
PIIMAGE="${STORAGE}/RaspberryPi/${SYSNAME}/${PIIMAGEZ}"

OFF_RAMP_TEXT="If this is not correct, use Ctrl+C to quit, or [Enter] to continue..."

clear

echo "=== Prepare $SYSNAME image for use at ACATBA. ===

This script assumes that these ACATBA resources are in ${STORAGE}:"
for s in $SOURCE_DIRS; do
    echo "${STORAGE}/ACATBA/${s}"
done

echo
read -p "$OFF_RAMP_TEXT"
echo

# Flash image to SD card.
device=''
if [[ -n "$1" ]]; then
    device="$1"
fi
skip=
if [[ "$2" == 'skip' ]]; then
    skip='skip'
fi
if [[ -z "$device" ]]; then
    echo "About to flash the $SYSNAME image to the given device. Use carefully!!!"
    echo "Or use \"skip\" argument to continue to system configuration."
    read -p "Give destination device (e.g. /dev/sda or /dev/mmcblk0 [skip]): " ans
    echo
    device=$(echo "$ans" | awk '{print $1}')
    skip=$(echo "$ans" | awk '{print $2}')
fi

if [[ "$skip" != 'skip' ]]; then
    if [[ -e ${device} ]]; then
        read -p "About to run command:
unzip -p "$PIIMAGE" | sudo dd of="${device}" bs=1M status=progress

Are you sure? [y/N]: " ans
        if [[ $ans == 'y' || $ans == 'Y' ]]; then
            echo "Unmounting $device."
            sudo umount ${device}*
            echo -e "\nCopying image."
            unzip -p "$PIIMAGE" | sudo dd of="${device}" bs=1M status=progress
            echo
        else
            echo "dd aborted. Exiting."
            exit 1
        fi
    else
        echo "Device not found, try again."
        exit 1
    fi
else
    # Assume image already written.
    echo "Skipped image flash for ${device}."
    sudo umount ${device}*
    echo
fi

### Select SD card device.
echo "Preparing to expand rootfs partition."
while [[ ! -e $device ]]; do
    echo "device: $device"
    read -p "Please type [unmounted] device path [e.g. /dev/sda]: " device
done

part_base=''
if [[ $device =~ /dev/mmcblk[0-9] ]]; then
    part_base=${device}p
elif [[ $device =~ /dev/sd[a-z] ]]; then
    part_base=${device}
fi
part1=${part_base}1
part2=${part_base}2

if [[ ! -n $part_base ]]; then
    read -p "Please enter path to boot partition [e.g. /dev/sda1]: " part1
    read -p "Please enter path to rootfs partition [e.g. /dev/sda2]: " part2
fi

echo -e "device:\t$device
boot:\t$part1
rootfs:\t$part2
"
read -p "$OFF_RAMP_TEXT"

### Expand rootfs to fill disk.
last_sector=$(sudo fdisk -o End --list "${device}" | tail -n1)
sudo parted -s "${device}" resizepart 2 '100%'
sudo e2fsck -f $part2
sudo resize2fs $part2

### Mount SD card boot partition.
part_name=$(lsblk --noheadings --output LABEL "$part1")
part_type=$(lsblk --noheadings --output FSTYPE "$part1")
mt_dir="${MNTHOME}/$part_name"
echo "Mounting $part1 to $mt_dir."
sudo mkdir -p "${mt_dir}"
sudo mount -t "${part_type}" "${part1}" "${mt_dir}"

### Mount SD card rootfs partition.
part_name=$(lsblk --noheadings --output LABEL "$part2")
part_type=$(lsblk --noheadings --output FSTYPE "$part2")
mt_dir="${MNTHOME}/$part_name"
echo "Mounting $part2 to $mt_dir."
sudo mkdir -p "${mt_dir}"
sudo mount -t "${part_type}" "${part2}" "${mt_dir}"
echo

### Set timezone.
echo "Setting timezone."
# NOTE: Symlink targets need to be relative to the created symlink file
#   if dealing with a mounted filesystem.
sudo ln -fs "../usr/share/zoneinfo/Africa/Bangui" "${PIROOTFS}/etc/localtime"
# /tmp/bdate script, read at ___ by sudo crontab
# date -s '".$DATESTR." ".$TIMESTR.":00'\n ### 19 APR 2012 11:14:00

### Ensure Captive Portal.
echo "Ensuring captive portal."
sudo ln -fs "../home/pi/conf/etc/dnsmasq_port.conf" "${PIROOTFS}/etc/dnsmasq.conf"

### Ensure French interface.
echo "Ensuring French interface."
sudo sed -Ei.bak "s|(\"LNG\" => )\".{2}\"|\1\"FR\"|" "${PICONF}/lighttpd/env"
sudo cp "${PIHOME}/FR_Content/"*.* "${PISITE}/content"

### Copy ACATBA content to Shared folder
echo "Copying ACATBA content to SD card."
o=( "--recursive" "--update" "--info=progress2" )
for d in $SOURCE_DIRS; do
    sudo rsync ${o[@]} "${STORAGE}/ACATBA/${d}" "${PISITE}/Shared"
done
echo -en "\nWaiting for file copy to finish..."
sync
echo -e " done.\n"

### Ensure SSID."
echo "Setting SSID to ${SSID}."
sudo sed -E -i.bak "s|^(ssid=).*$|\1${SSID}|" "${PICONF}/hostapd/hostapd.conf"
sudo sed -E -i.bak "s|^(friendly_name=).*$|\1${SSID}|" "${PICONF}/etc/minidlna.conf"

### Ensure hostname.
echo "Setting hostname to ${HOSTNAME}."
echo "$HOSTNAME" | sudo tee "${PICONF}/etc/hostname" >/dev/null
sudo sed -E -i.bak "s|^(127.0.1.1\t).*$|\1${HOSTNAME}|" "${PICONF}/etc/hosts"
sudo sed -Ei "s|^(192.168.50.1\t).*(\.local)|\1${HOSTNAME}\2|" "${PICONF}/etc/hosts"
sudo sed -E -i.bak "s|(Redirect to ).*(</title>)|\1${HOSTNAME}\2|" "${PISITE}/index.html"
sudo sed -Ei "s|(url=http://).*(.local)|\1${HOSTNAME}\2|" "${PISITE}/index.html"
sudo sed -E -i.bak "s|^(.*192.168.50.1) .*\.local$|\1 ${HOSTNAME}.local|" "${PICONF}/avahi/hosts"
sudo sed -E -i.bak "s|([\"/])[a-z-]+(\.local[\"/])|\1${HOSTNAME}\2|g" "${PICONF}/lighttpd/lighttpd_cap_port.conf"
sudo sed -E -i "s|^(presentation_url=).*$|\1http://${HOSTNAME}.local/Shared|" "${PICONF}/etc/minidlna.conf"
sudo sed -E -i.bak "s|^(address=).*$|\1/${HOSTNAME}.local/192.168.50.1|" "${PICONF}/etc/dnsmasq_noport.conf"

### Unmount partitions and remove mount directories.
sleep 1 # added delay to hopefully avoid "target is busy" error
for mt_dir in $(lsblk --output MOUNTPOINT "${device}" | tail -n+3); do
    sudo umount "${mt_dir}"
    if [[ $? -ne 0 ]]; then
        echo "Could not unmount. Exiting."
        exit 1
    fi
    sudo rm -df "${mt_dir}"
done

echo "
index:  http://${HOSTNAME}.local
config: http://${HOSTNAME}.local/controls/control.php
LAN IP: 192.168.50.1
user:   pi
pwd:    bbox100

The SD card is ready and unmounted."

exit 0
