#!/bin/bash

#===============================================================================
# Backup to Ext HD using rdiff-backup when other app not available.
# - assumes Ext HD has "ACATBA back" in partition name
#===============================================================================

# ------------------------------------------------------------------------------
# Inital setup.
# ------------------------------------------------------------------------------
# Ensure running as root.
if [[ $(id -u) -ne 0 ]]; then
    echo "ERROR: Need to run as sudo."
    exit 1
fi

# Define variables.
LANG=C
SHELL=/bin/bash
base_dir="/home/acatbadmin/Partages"
dest_dir="/mnt/servacatba-rdiff-backups"
log=/var/log/servacatba-rdiff-backup.log
touch "$log"

# Set debug output.
if [[ "$1" == '-d' || "$1" == '--debug' ]]; then
    set -x
    log=/dev/null # avoid logging debug info
fi

# Define functions.
clean_exit() {
    # Unmount backup drive.
    echo "Ensuring that backup drive is unmounted." | tee -a "$log"
    umount /mnt | tee -a "$log"

    # Finalize log entry.
    echo -e "Backup ended: $(date +'%Y-%m-%d %H:%M')\n\n" | tee -a "$log"
    exit $1
}

# ------------------------------------------------------------------------------
# Initialize log entry.
# ------------------------------------------------------------------------------
echo "Backup started: $(date +'%Y-%m-%d %H:%M')" | tee -a "$log"

# Ensure that rdiff-backup is installed.
if [[ ! $(which rdiff-backup) ]]; then
    echo "Installing rdiff-backup..." | tee -a "$log"
    apt-get --yes update
    apt-get --yes install rdiff-backup
    if [[ $? -ne 0 ]]; then
        echo "ERROR: Could not install rdiff-backup." | tee -a "$log"
        clean_exit 1
    fi
fi

# ------------------------------------------------------------------------------
# Ensure that cronjob is configured.
# ------------------------------------------------------------------------------
rdiffjob='/etc/cron.d/rdiff-backup'
script_path=$(realpath $0)
# Run job 45min after every hour.
cat >"$rdiffjob" << MULTILINE
45 * * * * root "$script_path"
MULTILINE

# ------------------------------------------------------------------------------
# Verify the destination.
# ------------------------------------------------------------------------------
# Ensure that a backup disk is attached. If >1, only handle 1st one.
backup_device=$(lsblk --fs --list --include=8 --noheadings | grep -i 'ACATBA back' | head -n1)
echo "Backup device: $backup_device" | tee -a "$log"
if [[ -z "$backup_device" ]]; then
    # No backup disks attached.
    echo "ERROR: No backup disks attached." | tee -a "$log"
    clean_exit 1
fi

# Ensure that backup disk is mounted.
if [[ ! $(echo "$backup_device" | awk '{print $6}') ]]; then
    # Backup disk(s) attached but not mounted.
    partition=$(echo "$backup_device" | awk '{print $1}')
    echo "Mounting /dev/${partition} to /mnt..." | tee -a "$log"
    mount /dev/${partition} /mnt
    if [[ $? -ne 0 ]]; then
        echo "ERROR: Disk not mounted." | tee -a "$log"
        clean_exit 1
    fi
fi

# Ensure that backup dir exists.
if [[ ! -d "$dest_dir" ]]; then
    echo "Ensuring $dest_dir exists."  | tee -a "$log"
    mkdir -p "$dest_dir"
fi

# ------------------------------------------------------------------------------
# Backup files.
# ------------------------------------------------------------------------------
echo "Backup destination: $dest_dir" | tee -a "$log"
opts=(
    --exclude-symbolic-links
    --override-chars-to-quote='\"*/:<>?\\\\|'
)

# Skip backups if already running.
if [[ $(pgrep rdiff-backup) ]]; then
    echo "WARNING: Backup already in progress."
    clean_exit 1
fi

# Backup default folders.
echo "Backing up standard folders:"  | tee -a "$log"
while IFS= read -r -d '' dir; do
    name=$(basename "$dir")
    echo "  $dir" | tee -a "$log"
    rdiff-backup -v1 "${opts[@]}" "${dir}" "${dest_dir}/${name}" | tee -a "$log"
done < <(find "$base_dir" -maxdepth 1 -name '*backup' -type d -print0)

# Backup user-added folders.
backup_dirs_list=$(dirname "${script_path}")/data/rdiff-backup/backup-dirs.txt
if [[ -n $(cat "$backup_dirs_list" | grep -E -v -e '^\s*#' -e '^\s*$') ]]; then
    echo "Backing up extra folders:"  | tee -a "$log"
    while read -r line; do
        # Skip empty lines and lines that begin with "#".
        if [[ -z "$line" || $(echo "$line" | grep '^\s*#') ]]; then
            continue
        fi
        # Ensure folder exists.
        if [[ ! -d "$line" ]]; then
            echo "  Skipping non-existant folder: $line" | tee -a "$log"
            continue
        fi
        name=$(basename "$line")
        echo "  $line" | tee -a "$log"
        rdiff-backup -v1 "${opts[@]}" "${line}" "${dest_dir}/${name}" | tee -a "$log"
    done < "$backup_dirs_list"
fi

# Remove old backups.
echo -e "\nRemoving stale backups..." | tee -a "$log"
while IFS= read -r -d '' dir; do
    name=$(basename "$dir")
    echo "  Checking: $dir" | tee -a "$log"
    rdiff-backup -v1 --remove-older-than=6M "${dest_dir}/${name}" | tee -a "$log"
done < <(find "$dest_dir" -maxdepth 1 -name '*backup' -type d -print0)

clean_exit 0
