#!/bin/bash

# ==============================================================================
# - This script is used to install and configure the unifi controller:
#	- Ensure dependencies are met
#	- Ensure proper configuration
#
# ==============================================================================
SCRIPT="${0}"
PARENT=$(dirname "${0}")

# ------------------------------------------------------------------------------
# Ensure elevated privileges.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}"
    exit $?
fi

echo "Attention ! Ce script installera et configurera le contrôlleur unifi."
# read -p "Tappez [Enter] pour continuer ou Ctrl+C pour annuler..."

# ------------------------------------------------------------------------------
# Ensure Admin user.
# ------------------------------------------------------------------------------
ADMIN=it_admin
if ! grep $ADMIN /etc/passwd >/dev/null; then
    adduser --gecos 'it_admin,,,' --disabled-login --uid 1999 $ADMIN
fi
adduser $ADMIN adm
adduser $ADMIN dialout
adduser $ADMIN dip
adduser $ADMIN netdev
adduser $ADMIN plugdev
adduser $ADMIN sudo
# Set 1-time password if not already set (status=P if set).
status=$(passwd --status $ADMIN | awk '{print $2}')
if [[ $status != P ]]; then
    echo -e 'password\npassword' | passwd $ADMIN
    # Force password to expire immediately.
    passwd -e $ADMIN
fi

# ------------------------------------------------------------------------------
# Ensure necessary packages.
# ------------------------------------------------------------------------------
if [[ ! -x /usr/lib/unifi/bin/unifi.init ]]; then
    echo "Installation de unifi et ses dépendences."
    apt-get update
    apt-get install --yes apt-transport-https

    if [[ "$(arch)" == 'aarch64' ]]; then
        repo_line="deb [arch=armhf] https://www.ui.com/downloads/unifi/debian stable ubiquiti"
    else
        repo_line="deb https://www.ui.com/downloads/unifi/debian stable ubiquiti"
    fi
    echo "$repo_line" | tee /etc/apt/sources.list.d/100-ubnt-unifi.list
    wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg

    apt-get update
    apt-get install --yes openjdk-8-jre-headless unifi

    systemctl restart unifi.service # maybe needed if properties file isn't created fast enough
    systemctl status --no-pager --full mongodb.service unifi.service
fi


# ------------------------------------------------------------------------------
# Ensure configuration.
# ------------------------------------------------------------------------------
unifi_src="${PARENT}/data/unifi"
ipv4=$(ip -br a | grep '172\.16' | awk '{print $3}' | awk -F '/' '{print $1}')
# Check if config has been set.
if [[ ! -f "${unifi_src}/acatba-config-set" ]]; then
    # Apparently the config needs to be "restored" from a backup file using the GUI.
    #   This file is found at servers/data/unifi/*.unf.
    conf_file=$(find "${unifi_src}/" -name '*.unf' | sort | head -n1)
    echo "Pour remettre la configuration sauvegardée, veuillez lancer le controlleur :"
    echo " - Allez à $ipv4:8443"
    echo " - Choisissez « Restore Backup from a file »"
    echo " - Choisissez le ficher qui se trouve à $conf_file"
    read -p "Tappez [Enter] après avoir ."
    touch "${unifi_src}/acatba-config-set"
fi

# Ensure crontab file.
unifi_cron="/etc/cron.d/unifi-jobs"
echo "Installation de $unifi_cron."
cp "${unifi_src}/unifi-jobs" "$unifi_cron"
