### ACATBA servacatba setup scripts
The scripts in this repo are intended to be used for the proper configuration of the various services of ACATBA's "servacatba" server. It's intentionally composed of multiple scripts so that individual parts could be split off and installed independently on other servers.

## Detachable ssh commands

# Send command to background

If the command doesn't have to be scripted, you can do it this way:

1. run it in the foreground
1. pause it (CTRL+Z), note the jobid in "[]"
1. disown it so that it won't be closed when you close your shell
  ```shell
  $ disown -h %jobid
  ```
1. resume the job in the background
  ```shell
  $ bg %jobid
  ```
1. if needed, bring it back to the foreground
  ```shell
  $ fg %jobid`
  ```

# Use screen

```shell
$ ssh user@ip screen -dm -S "name"
```
