#!/bin/bash

# ==============================================================================
# - This script is used to configure the vanilla OS to fit the specific
#   conditions for running any services on the server on ACATBA's network,
#   including:
#   - default installed apps
#   - French locale
# - It assumes a standard Ubuntu 20.04 server system as a starting point.
# ==============================================================================
PARENT=$(dirname "${0}")
# Set username (some files in ./data are hardcoded to use "acatbadmin").
ADMIN=acatbadmin
REPO_SERIES=$(lsb_release -sc)

# ------------------------------------------------------------------------------
# Ensure running as root but also preserving environment.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    # Need to relaunch with sudo and with environment preserved.
    sudo "${0}"
    exit $?
fi

# echo "Attention ! Ce script configurera le système comme serveur. Ça va durer."
# read -p "Tappez [Enter] pour continuer ou Ctrl+C pour annuler..."


# ------------------------------------------------------------------------------
# Ensure Admin user.
# ------------------------------------------------------------------------------
adduser --gecos 'ACATBAdmin,,,' --disabled-login --uid 1999 $ADMIN
adduser $ADMIN adm
adduser $ADMIN cdrom
adduser $ADMIN sudo
adduser $ADMIN plugdev
# Set 1-time password if not already set (status=P if set).
status=$(passwd --status $ADMIN | awk '{print $2}')
if [[ $status != P ]]; then
    echo -e 'password\npassword' | passwd $ADMIN
    # Force password to expire immediately.
    passwd -e $ADMIN
fi


# ------------------------------------------------------------------------------
# Set software sources.
# ------------------------------------------------------------------------------
# APT_SOURCES_D=/etc/apt/sources.list.d
# if [ -x /usr/bin/wasta-offline ] &&  [[ $(pgrep -c wasta-offline) > 0 ]]; then
#     if [ -e /etc/apt/sources.list.d.wasta ]; then
#         echo "*** wasta-offline 'offline only' mode detected"
#         echo
#         APT_SOURCES_D=/etc/apt/sources.list.d.wasta
#     else
#         echo "*** wasta-offline 'offline and internet' mode detected"
#         echo
#     fi
# fi

# Remove keyman deb repo.
#rm --force "${APT_SOURCES_D}/keymanapp-ubuntu-keyman-${REPO_SERIES}.list" || true

# Remove cinnamon deb repo.
#rm --force "${APT_SOURCES_D}"/wasta-linux-ubuntu-cinnamon-*.list

# Disable skypeforlinux deb repo.
#rm --force "${APT_SOURCES_D}/skype-stable.list"

# Notify me of a new Ubuntu version: never, normal, lts
#   (note: apparently /etc/update-manager/release-upgrades.d doesn't work)
if [ -e /etc/update-manager/release-upgrades ]; then
    sed -i -e 's|^Prompt=.*|Prompt=never|' /etc/update-manager/release-upgrades
fi

# disable downloading of DEP-11 files.
#   alternative is apt purge appstream - then you lose snaps/ubuntu-software
dpkg-divert --local --rename --divert '/etc/apt/apt.conf.d/#50appstream' /etc/apt/apt.conf.d/50appstream


# ------------------------------------------------------------------------------
# Ensure that correct base software is installed.
# ------------------------------------------------------------------------------
# Wait for locks to be released.
if [[ $(sudo lslocks -n -o COMMAND | grep unattended-upgr) ]]; then
    echo "Waiting for unattended-upgr to finish."
    while [[ $(sudo lslocks -n -o PATH | grep /var/lib/dpkg/lock) ]]; do
        echo -n "."
        sleep 3
    done
    echo
fi

# Ensure language support.
#apt-get --yes install $(check-language-support)

# Install/update added debs.
debs=(
    #firefox
    #firefox-locale-fr
    #gnome-getting-started-docs-fr
    #gnome-user-docs-fr
    #hunspell-fr
    #language-pack-fr
    #language-pack-fr-base
    #language-pack-gnome-fr
    #language-pack-gnome-fr-base
    nbtscan
    nmap
    python3-bs4
    python3-packaging
    squid-deb-proxy-client
    #tigervnc-standalone-server
    #wfrench
    #snapd
    #wasta-gnome-focal
    )
apt-get update
for deb in "${debs[@]}"; do
    # Skip commented-out packages.
    if [[ ${deb:0:1} == '#' ]]; then
        continue
    fi
    apt-get --yes install "$deb"
done

# Reboot after adding wasta-gnome desktop.
#if [[ $DESKTOP_SESSION != 'wasta-gnome' ]]; then
#    echo "Changement d'environnement de bureau."
#    read -p "Tappez [Enter] pour redémarrer, puis connectez-vous avec \"Wasta GNOME\"."
#	reboot
#fi

# Install added snaps.
#snaps=(
#    #"chromium"
#    )
#for snap in "${snaps[@]}"; do
#    snap install "$snap"
#done

# Set wasta-snap-manager's suggested update defaults.
if [ $(which snap) ]; then
    snap set system refresh.metered=hold
    snap set system refresh.timer='sun5,02:00'
    snap set system refresh.retain=2
fi

# Remove unwanted debs.
removals=(
    #art-of-reading3
    #audacity
    #bloom-desktop
    #bookletimposer
    #calibre
    #cheese*
    #cinnamon
    #diodon
    #easytag
    #firefox
    #gimp
    #goldendict
    #inkscape
    #klavaro
    #libreoffice*
    #gnome-mahjongg
    #gnome-mines
    #rhythmbox
    #shotwell
    #snapd
    #gnome-sudoku
    #thunderbird
    #vlc
    )
apt-get --yes purge "${removals[@]}"

# Remove orphaned packages.
apt-get --yes autoremove

# Update installed packages.
apt-get update
apt-get --yes dist-upgrade


# ------------------------------------------------------------------------------
# Ensure hostname.
# ------------------------------------------------------------------------------
if [[ ! $(hostnamectl status | grep '   Static hostname: servacatba') ]]; then
    hostnamectl set-hostname servacatba
fi


# ------------------------------------------------------------------------------
# Ensure system-wide locale settings.
# ------------------------------------------------------------------------------
# Set timezone.
if [[ "$REPO_SERIES" == 'bionic' ]]; then
    timezone=$(timedatectl status | grep 'Time zone:' | awk -F':' '{print $2}' | awk '{print $1}')
else
    # Assume focal or newer.
    timezone=$(timedatectl show --property=Timezone | awk -F'=' '{print $2}')
fi
if [[ "$timezone" != "Africa/Bangui" ]]; then
    timedatectl set-timezone "Africa/Bangui"
fi

# Ensure French locale settings.
if [[ $LC_ALL != "fr_FR.UTF-8" ]]; then
    # Generate the French (France) locale.
    locale-gen fr_FR.UTF-8

    update-locale LANG="fr_FR.UTF-8"
    update-locale LANGUAGE="fr_FR"
    update-locale LC_ALL="fr_FR.UTF-8"
fi


# ------------------------------------------------------------------------------
# Configure TigerVNC.
# ------------------------------------------------------------------------------
# Set user config.
#vnc_dir="/home/$ADMIN/.vnc"
#sudo --user=$ADMIN mkdir -p "${vnc_dir}"
#conf_file="${vnc_dir}/vnc.conf"
#if [[ ! -e $conf_file ]]; then
#    cp "${PARENT}/data/vnc/vnc.conf" "${conf_file}"
#    chown $ADMIN:$ADMIN "${conf_file}"
#fi

# Create password file.
#passwd_file="${vnc_dir}/passwd"
#if [[ ! -e $passwd_file ]]; then
#    sudo --user=$ADMIN touch "${passwd_file}"
#    chmod 600 "${passwd_file}"
#    echo "$ADMIN" | tigervncpasswd -f > "${passwd_file}"
#fi

# Create Xvnc-session file.
#xsess_file="${vnc_dir}/Xvnc-session"
#if [[ ! -e $xsess_file ]]; then
#    sudo --user=$ADMIN touch "${xsess_file}"
#    cp "${PARENT}/data/vnc/Xvnc-session" "${xsess_file}"
#    chown $ADMIN:$ADMIN "${xsess_file}"
#fi

# Create systemd service.
#   Got lots of help from here:
#   https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/3038
#svc="vncserver@.service"
#svc_file="/etc/systemd/system/${svc}"

# Copy systemd unit file for focal.
#if [[ ! -e $svc_file ]]; then
#    cp "${PARENT}/data/vnc/${svc}" "${svc_file}"
#    if [[ $REPO_SERIES = 'bionic' ]]; then
#        # Overwrite with systemd unit file for bionic.
#        cp "${PARENT}/data/vnc/${svc}.bionic" "${svc_file}"
#    fi
#fi

# Reload, stop, and/or start the service.
#systemctl daemon-reload
#svc_name="vncserver@1.service"
#if [[ $(systemctl is-active "${svc_name}") ]]; then
#    systemctl stop "${svc_name}"
#fi
#systemctl enable --now "${svc_name}"

# Verify systemd service.
#vnc_conns=
#ct=0
#while [[ ! $vnc_conns ]]; do
#    if [[ $ct -gt 100 ]]; then
#        echo "Serveur VNC n'a pas bien démarré. Veuillez en vérifier la configuration."
#        exit 1
#    fi
#    sleep 0.1
#    vnc_conns=$(netstat -nplt | grep ':5901')
#    ct+=1
#done

# Output relevant confirmation details.
#device=$(ip route | grep default | awk '{print $5}')
#ip=$(ip route get 1.1.1.1 | grep via | awk '{print $7}')
#echo "VNCserver en marche à ${ip}:5901."


# ------------------------------------------------------------------------------
# End of script. Reboot if necessary.
# ------------------------------------------------------------------------------
if [[ -f /var/run/reboot-required ]]; then
	read -p "End of script. Tap [Enter] to restart the system."
	reboot
fi
