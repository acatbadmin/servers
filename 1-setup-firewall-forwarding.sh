#!/bin/bash

# ==============================================================================
# - This script is used to forward the Zywall firewall page to servacatba.
# ==============================================================================
PARENT=$(dirname "${0}")
SCRIPTS="${PARENT}/scripts"
DATA="${PARENT}/data"


# ------------------------------------------------------------------------------
# Ensure elevated privileges.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}"
    exit $?
fi

echo "Attention ! Ce script rendra la page du pare-feu disponible à l'adresse du server."
read -p "Tappez [Enter] pour continuer ou Ctrl+C pour annuler..."


# ------------------------------------------------------------------------------
# Ensure firewall page forwarding.
# ------------------------------------------------------------------------------
# Ensure forwarding script is in place.
local_bins=/usr/local/bin
fwd_script="fwd-firewall-page.sh"
if [[ ! $(which $fwd_script) ]]; then
    echo "Copie de ${SCRIPTS}/$fwd_script à $local_bins."
    mkdir -p "$local_bins"
    cp "${SCRIPTS}/$fwd_script" "$local_bins"
fi

# Ensure forwarding script runs at reboot.
svc_name="fwd-firewall-page.service"
svc_file="/etc/systemd/system/$svc_name"
if [[ ! -e $svc_file ]]; then
    echo "Installation de $svc_file."
    cp "${DATA}/firewall-forwarding/$svc_name" "$svc_file"
    systemctl daemon-reload
    systemctl enable $svc_name
fi

# Run the forwarding script now, if needed.
if [[ ! $(iptables -t nat --list | grep -e DNAT -e SNAT) ]]; then
    echo "Activation de « forwarding » de la page du pare-feu."
    systemctl start $svc_name
fi
