#!/bin/bash

# ==============================================================================
# Ensure installation and proper configuration of a wasta-2004 container on lxd,
#   so that its updates can be cached by the host's squid-deb-proxy instance.
#   NOTE: The network configuration for this container only allows for its
#       updates to be cached if the squid-deb-proxy server is the host, not
#       another machine on the LAN.
#
# This script assumes an Ubuntu Server 20.04 host, but should also work on other
#   releases.
#   - Ensure installation of snapd, lxd.            (100 MB)
#   - Configure container with bridged connection.
#   - Install Wasta 20.04 desktop:
#     - Install ubuntu-20.04 container.             (400 MB)
#     - Install wasta-core                          (75 MB)
#     - Install other base packages.                (1500 MB)
#   - Install CAR-specific packages.                (a few MB?)
#   - Periodically launch container, perform upgrades, shutdown.
# ==============================================================================
SCRIPT="${0}"
PATH=$PATH:/snap/bin
PARENT_DIR=$(dirname $(realpath $0))
LXD_DATA_DIR="${PARENT_DIR}/data/lxd"
LXD_STORAGE='wasta2004'
NAME='wasta-2004'
WASTA_USER='wasta'
cont_home="/home/${WASTA_USER}"
# This list is necessarily bigger that wasta-custom-car dependencies.
CAR_PKGS="hplip-gui paratext-9.0 wasta-custom-car"


# Give timestamp.
echo -e "\nStarting time: $(date)"

# Ensure installation of snapd.
if [[ ! $(which snap) ]]; then
    echo "Updating and installing snap & zfs-utils packages..."
    sudo apt-get --yes update
    sudo apt-get --yes upgrade
    sudo apt-get --yes install snap zfsutils-linux
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install snap & zfs-utils. Exiting."
        exit 1
    fi
fi

# Ensure installation of lxd.
if [[ ! $(which lxd) ]]; then
    echo "Installing lxd..."
    sudo snap install lxd
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install lxd snap. Exiting."
        exit 1
    fi
    # Add user to lxd group.
    sudo usermod -a -G lxd $(whoami)
fi

# Ensure initialization of lxd (including storage bin).
if [[ ! $(lxc storage list | grep $LXD_STORAGE) ]]; then
    echo "Initializing lxd..."
    preseed_file="${LXD_DATA_DIR}/lxd-wasta-updates-preseed.yaml"
    cat "$preseed_file" | lxd init --preseed
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to init lxd. Exiting."
        exit 1
    fi
fi

# Ensure that nano is the default editor (helps with lxc).
prof_line="export EDITOR=nano"
if [[ ! $(grep "$prof_line" ~/.profile) ]]; then
    echo "$prof_line" >> ~/.profile
    source ~/.profile
fi

# Ensure wasta server container.
if [[ ! $(lxc list | grep " $NAME ") ]]; then
    echo "Installing $NAME container..."
    lxc launch ubuntu:20.04 "$NAME" --verbose --profile=wasta-2004
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to create $NAME container. Exiting."
        exit 1
    fi
    # Create post-launch snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "1-container-created"
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 5
fi

# Ensure container is running.
if [[ $(lxc info "$NAME" | grep Status | awk '{print $2}') != 'RUNNING' ]]; then
    echo "Starting $NAME container..."
    lxc start "$NAME"
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to start $NAME containter. Exiting."
        exit 1
    fi
    # Show file system usage.
    lxc exec "$NAME" -- df -h /
    echo
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 15
fi

# Ensure static IP.
yaml_name="99-acatba-static-ip-31.yaml"
yaml_exists=$(lxc exec "$NAME" -- find /etc/netplan -name "$yaml_name")
if [[ ! $yaml_exists ]]; then
    echo "Configuring netplan for static IP..."
    lxc file push "${LXD_DATA_DIR}/${yaml_name}" "${NAME}/etc/netplan/${yaml_name}"
    lxc exec "$NAME" -- netplan apply
    echo "Updated network details:"
    lxc exec "$NAME" -- ip -br a
    echo
fi

# Ensure user on container.
if [[ ! $(lxc exec "$NAME" -- grep $WASTA_USER /etc/passwd) ]]; then
    # Create wasta user.
    echo "Creating $WASTA_USER user..."
    lxc exec "$NAME" -- adduser --gecos 'Wasta,,,' --disabled-login --uid 1998 $WASTA_USER
    lxc exec "$NAME" -- adduser $WASTA_USER adm
    lxc exec "$NAME" -- adduser $WASTA_USER sudo
    # Set 1-time password if not already set (status=P if set).
    status=$(lxc exec "$NAME" -- passwd --status $WASTA_USER | awk '{print $2}')
    if [[ $status != P ]]; then
        echo -e 'password\npassword' | lxc exec "$NAME" -- passwd $WASTA_USER
    fi
    # Create user-config snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "2-user-created"
fi

# Ensure installation of squid-deb-proxy.
pkg="squid-deb-proxy-client"
if [[ ! $(lxc exec "$NAME" -- dpkg -l | grep ^ii | grep $pkg) ]]; then
    echo "Updating packages list..."
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 5
    lxc exec "$NAME" -- apt-get update
    # Install package.
    echo "Installing $pkg..."
    lxc exec "$NAME" -- apt-get --yes install $pkg
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install $pkg. Exiting."
        exit 1
    fi
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "3-squid-deb-proxy-client-installed"
fi

# Ensure addition of Wasta system PPA.
list_name=wasta-linux-ubuntu-wasta-focal.list
wasta_list_check=$(lxc exec "$NAME" -- find /etc/apt/sources.list.d/ -name $list_name)
if [[ ! $wasta_list_check ]]; then
    echo "Adding Wasta system PPA..."
    # Manually add the PPA. (add-apt-repository fails, maybe due to proxy?)
    list=/etc/apt/sources.list.d/$list_name
    lxc file push "${LXD_DATA_DIR}/$list_name" "${NAME}${list}"
    key="A3433DEABCB572389D336893232A5F5626B47DF6"
    lxc exec "$NAME" -- apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "$key"
    lxc exec "$NAME" -- apt-get update
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to get updates after adding Wasta system PPA. Exiting."
        exit 1
    fi
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "4-wasta-ppa-added"
fi

# Ensure installation of wasta-core-focal package.
pkg="wasta-core-focal"
if [[ ! $(lxc exec "$NAME" -- dpkg -l | grep ^ii | grep $pkg) ]]; then
    echo "Installing $pkg..."
    lxc exec "$NAME" -- apt-get --yes install $pkg
    ec1=$?
    lxc exec "$NAME" -- apt-get update
    ec2=$?
    sum=$((ec1 + ec2))
    if [[ $((ec1 + ec2)) -ne 0 ]]; then
        echo -e "\nFailed to install $pkg. Exiting."
        exit 1
    fi
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "5-wasta-core-installed"
fi

# Ensure wasta-initial-setup has been run.
if [[ ! $(lxc exec "$NAME" -- find /var/log/ -name wasta-initial-setup-done) ]]; then
    echo "Running wasta-initial-setup..."
    lxc exec "$NAME" -- apt-get update

    # Ignore snapd removal.
    app_rem=/usr/share/wasta-core/scripts/app-removals.sh
    lxc exec "$NAME" -- cp "$app_rem" "${app_rem}.orig"
    lxc exec "$NAME" -- sed -i 's|^pkgToRemoveListFull="snapd"|pkgToRemoveListFull=""|' "$app_rem"

    # Leave zswap disabled.
    en_zswap=/usr/bin/wasta-enable-zswap
    lxc exec "$NAME" -- cp "$en_zswap" "${en_zswap}.orig"
    lxc exec "$NAME" -- sed -i 's|/etc/default/grub|/dev/null|' "$en_zswap"

    # Swap out app-installs.sh script.
    setup="/usr/bin/wasta-initial-setup"
    lxc exec "$NAME" -- cp "$setup" "${setup}.orig"
    new_app_installs="${cont_home}/app-installs.sh"
    lxc file push "${LXD_DATA_DIR}/wasta-2004.1-desktop-base-pkgs.list" "${NAME}${cont_home}/wasta-pkgs.list"
    lxc file push "${LXD_DATA_DIR}/app-installs.sh" "${NAME}${new_app_installs}" --mode 555
    lxc exec "$NAME" -- sed -i 's|bash $DIR/scripts/app-installs.sh $AUTO|bash '"$new_app_installs"'|' "$setup"

    # Preseed debconf selections.
    lxc file push "${LXD_DATA_DIR}/debconf.txt" "${NAME}${cont_home}/debconf.txt"
    lxc exec "$NAME" -- debconf-set-selections --verbose "${cont_home}/debconf.txt"

    # Run the script.
    lxc exec "$NAME" -- wasta-initial-setup auto
    if [[ $? -eq 0 ]]; then
        lxc exec "$NAME" -- touch /var/log/wasta-initial-setup-done
    else
        echo -e "\nFailed to complete wasta-initial-setup. Exiting."
        exit 1
    fi
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "6-wasta-initial-setup-done"
fi

# Remove ubuntu user: it's UID is the same as wasta-car admin's UID!
if [[ $(grep ^ubuntu /etc/passwd) ]]; then
    echo "Removing Ubuntu user..."
    lxc exec "$NAME" -- deluser --remove-home ubuntu
    if [[ $(grep ^ubuntu /etc/group) ]]; then
        echo "Removing Ubuntu group..."
        lxc exec "$NAME" -- delgroup ubuntu
    fi
fi

# Create dummy skype snap executable to avoid it really being installed.
lxc exec "$NAME" -- touch /snap/bin/skype

# Ensure CAR-specific packages are installed.
for pkg in ${CAR_PKGS[@]}; do
    if [[ ! $(lxc exec "$NAME" -- dpkg -l | grep ^ii | grep $pkg) ]]; then
        echo "Installing $pkg..."
        # Install package.
        lxc exec "$NAME" -- apt-get --yes install $pkg
        if [[ $? -ne 0 ]]; then
            echo -e "\nFailed to install $pkg. Exiting."
            exit 1
        fi
    fi
done


# Restart container if needed.
if [[ $(lxc exec "$NAME" -- find /var/run/ -name 'reboot-required' 2>/dev/null) ]]; then
    echo "Restarting $NAME container..."
    lxc restart "$NAME"
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 5
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "7-wasta-custom-car-installed"
fi

# Add script to daily cronjob.
update_script="wasta-2004-update-check"
if [[ ! -x /etc/cron.daily/${update_script} ]]; then
    sudo cp ${LXD_DATA_DIR}/${update_script} /etc/cron.daily
fi

# Update wasta-2004 container.
echo "Creating snapshot..."
lxc snapshot "$NAME" "pre-updates" --reuse
echo "Updating packages..."
lxc exec "$NAME" -- apt-get update
lxc exec "$NAME" -- apt-get --yes dist-upgrade
lxc exec "$NAME" -- apt-get --yes autoremove

echo "Update complete. Shutting down."
# Stop the container.
lxc stop "$NAME"

# Give timestamp.
echo -e "Ending time: $(date)\n"
