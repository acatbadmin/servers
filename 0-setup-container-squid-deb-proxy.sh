#!/bin/bash

# TODO:
#   - make container accessible to host (lxdbr0?)

# ==============================================================================
# This script is used to install and configure squid-deb-proxy in an lxd
#   container.
#
# It assumes an Ubuntu Server 20.04 host, but should also work on other
#   releases.
#   - Ensure installation of snapd, lxd.            (100 MB)
#   - Install container (image, rootfs).            (500 MB)
#   - Configure container with macvlan connection.
#   - Install squid-deb-proxy.
#   - Disable squid.
#
# NOTE: To re-run lxd init (and restart this whole script) you must delete:
#   containers, profiles, networks, storage pools
#   $ lxc delete <container>
#   $ lxc profile delete <profile>
#   $ lxc network delete <network>
#   $ lxc storage delete <storage pool>
# ==============================================================================
SCRIPT="${0}"
PARENT_DIR=$(dirname $(realpath $0))
LXD_DATA_DIR="${PARENT_DIR}/data/lxd"
LXD_STORAGE='debproxy'
# WARNING: the 'deb-proxy' profile has the host's eth device hardcoded. This
#   script attempts to update this on the fly.
LAN_PROFILE='deb-proxy'
PRESEED='squid-deb-proxy-preseed.yaml'
NAME='deb-proxy'
ETH_DEV=$(ip -br addr | grep -Eow '^en[ps0-9]+' | head -n1)


# Give timestamp.
echo -e "\nStarting time: $(date)"

# Ensure that nano is the default editor (helps with lxc).
prof_line="export EDITOR=nano"
if [[ ! $(grep "$prof_line" ~/.profile) ]]; then
    echo "$prof_line" >> ~/.profile
    source ~/.profile
fi

# Ensure that existing software list is up-to-date.
sudo apt-get --yes update

# Ensure installation of snapd.
if [[ ! $(which snap) ]]; then
    echo "Installing snap package..."
    sudo apt-get --yes install snap
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install snap. Exiting."
        exit 1
    fi
fi

if [[ ! $(which zpool) ]]; then
    echo "Installing zfs-utils package..."
    sudo apt-get --yes install zfsutils-linux
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install zfs-utils. Exiting."
        exit 1
    fi
fi

# Ensure that all software is up-to-date.
sudo apt-get --yes upgrade
sudo apt-get --yes autoremove

# Ensure installation of lxd.
if [[ ! $(which lxd) ]]; then
    echo "Installing lxd..."
    sudo snap install lxd
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install lxd snap. Exiting."
        exit 1
    fi
    # Add user to lxd group.
    sudo usermod -a -G lxd $(whoami)
fi

# Ensure initialization of lxd (including storage bin).
if [[ ! $(lxc storage list | grep $LXD_STORAGE) ]]; then
    echo "Initializing lxd..."
    preseed_file="${LXD_DATA_DIR}/${PRESEED}"
    # Update preseed file with actual eth device.
    sed -r -i 's/parent: en[ps0-9]+/parent: '$ETH_DEV'/' "$preseed_file"
    cat "$preseed_file" | lxd init --preseed
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to init lxd. Exiting."
        exit 1
    fi
fi

# Ensure squid-deb-proxy container.
if [[ ! $(lxc list | grep "$NAME") ]]; then
    echo "Installing $NAME container..."
    lxc launch ubuntu:20.04 "$NAME" --verbose --profile=$LAN_PROFILE
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to create $NAME container. Exiting."
        exit 1
    fi

    # Create post-launch snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "1-container-created"
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 15
fi

# Ensure that ubuntu password is set (status=P if set).
status=$(lxc exec "$NAME" -- passwd --status ubuntu | awk '{print $2}')
if [[ $status != P ]]; then
    echo -e 'administrator\nadministrator' | lxc exec "$NAME" -- passwd ubuntu
fi

# Ensure ssh access.
conf_name="enable-ubuntu-ssh.conf"
src="${LXD_DATA_DIR}/${conf_name}"
ssh_conf_dir="/etc/ssh/sshd_config.d"
dst="${NAME}${ssh_conf_dir}/${conf_name}"
if [[ ! $(lxc exec "$NAME" -- find "$ssh_conf_dir" -name "$conf_name") ]]; then
    lxc file push "$src" "$dst"
    lxc exec "$NAME" -- systemctl restart sshd.service
fi

# Ensure container is running.
if [[ $(lxc info "$NAME" | grep Status | awk '{print $2}') != 'Running' ]]; then
    echo "Starting $NAME container..."
    lxc start "$NAME"
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to start $NAME containter. Exiting."
        exit 1
    fi
    # Show file system usage.
    lxc exec "$NAME" -- df -h /
    echo
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 15
fi

# Ensure static IP.
yaml_name="99-acatba-static-ip-30.yaml"
yaml_exists=$(lxc exec "$NAME" -- find /etc/netplan -name "$yaml_name")
if [[ ! $yaml_exists ]]; then
    echo "Configuring netplan for static IP..."
    lxc file push "${LXD_DATA_DIR}/${yaml_name}" "${NAME}/etc/netplan/${yaml_name}"
    lxc exec "$NAME" -- netplan apply
    echo "Updated network details:"
    lxc exec "$NAME" -- ip -br a
    echo
fi

# Ensure installation of squid-deb-proxy.
pkgs=("squid-deb-proxy" "squid-deb-proxy-client")
echo "Updating packages list..."
# Brute force delay to make sure container is reponsive to later commands.
sleep 5
lxc exec "$NAME" -- apt-get update
for pkg in "${pkgs[@]}"; do
    if [[ ! $(lxc exec "$NAME" -- dpkg -l | grep ^ii | grep $pkg) ]]; then
        # Install package.
        echo "Installing $pkg..."
        lxc exec "$NAME" -- apt-get --yes install $pkg
        if [[ $? -ne 0 ]]; then
            echo -e "\nFailed to install $pkg. Exiting."
            exit 1
        fi
    fi
done

# Configure allowed repos.
echo "Ensuring proper configuration of squid-deb-proxy..."
mirror_dir="/etc/squid-deb-proxy/mirror-dstdomain.acl.d"

# Allow sourceforge repo (libdvd-pkg).
libdvd_repos="${mirror_dir}/20-sourceforge-downloads"
lxc exec "$NAME" -- sh -c "echo downloads.sourceforge.net > $libdvd_repos"

# Configure to allow Wasta repos.
wasta_repos="${mirror_dir}/20-wasta-repos"
# Erase the existing file with the first entry.
lxc exec "$NAME" -- sh -c "echo ppa.launchpad.net > $wasta_repos"
lxc exec "$NAME" -- sh -c "echo packages.sil.org >> $wasta_repos"
lxc exec "$NAME" -- sh -c "echo repo.skype.com >> $wasta_repos" # Skype
lxc exec "$NAME" -- sh -c "echo dl.google.com >> $wasta_repos" # Chrome

# Flathub
flathub_repos="${mirror_dir}/20-flathub-repos"
# Erase the existing file with the first entry.
lxc exec "$NAME" -- sh -c "echo flathub.org > $flathub_repos"

# Configure to allow Unifi repos.
unifi_repos="${mirror_dir}/20-unifi-repos"
# Erase the existing file with the first entry.
lxc exec "$NAME" -- sh -c "echo repo.mongodb.org > $unifi_repos"
lxc exec "$NAME" -- sh -c "echo www.ui.com >> $unifi_repos"

# Configure to allow armbian repos.
armbian_repos="${mirror_dir}/20-armbian-repos"
# Erase the existing file with the first entry.
lxc exec "$NAME" -- sh -c "echo apt.armbian.com > $armbian_repos"
lxc exec "$NAME" -- sh -c "echo armbian.systemonachip.net >> $armbian_repos"
lxc exec "$NAME" -- sh -c "echo minio.k-space.ee >> $armbian_repos"
lxc exec "$NAME" -- sh -c "echo ports.ubuntu.com >> $armbian_repos"
lxc exec "$NAME" -- sh -c "echo mirrors.netix.net >> $armbian_repos"

# Configure to allow insync repo.
insync_repos="${mirror_dir}/20-insync-repos"
# Erase the existing file with the first entry.
lxc exec "$NAME" -- sh -c "echo apt.insync.io > $insync_repos"

# Restart squid-deb-proxy service.
echo "Restarting squid-deb-proxy.service (30 sec)..."
lxc exec "$NAME" -- systemctl restart squid-deb-proxy.service

# Stop and disable squid.service.
lxc exec "$NAME" -- systemctl status squid.service --no-pager 2>&1 >/dev/null
squid_status=$?
if [[ $squid_status -eq 0 ]]; then
    # Kill squid service.
    echo "Stopping and disabling squid.service (30 sec)..."
    lxc exec "$NAME" -- systemctl disable --now squid.service
fi
