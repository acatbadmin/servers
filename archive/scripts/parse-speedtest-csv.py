#!/usr/bin/python3

import argparse
import csv
import datetime
import statistics
import sys

from os import environ
from pathlib import Path


def valid_filetype(file_path):
    # validate file type
    return True if file_path.suffix == '.csv' else False

def valid_file(file_path):
    # validate file path
    return file_path.is_file()

def validate_file(file_path):
    '''
    Validate file name and path.
    '''
    if not valid_file(file_path):
        print(f"Error: Invalid file. Path {file_path} does not exist.")
        exit(1)
    elif not valid_filetype(file_path):
        print(f"Error: Invalid file type. {file_path} must be a .csv file.")
        exit(1)
    return

def get_default_infile():
    '''
    Set file_path by trying default options.
    '''
    file_paths = [
        Path(environ['HOME']) / 'speedtest.csv',
        Path(environ['HOME']) / 'Partages' / 'apps-etc' / 'speedtest.csv',
    ]
    good_path = False
    good_type = False
    for p in file_paths:
        if valid_file(p):
            good_path = True
        if valid_filetype(p):
            good_type = True
        if good_path and good_type:
            return str(p)
    return None

def validate_file(file_path):
    '''
    Ensure that file exists and is the correct type. Exit if not.
    '''
    if not valid_file(file_path):
        print(f"Error: Invalid file. Path {file_path} does not exist.")
        exit(1)
    elif not valid_filetype(file_path):
        print(f"Error: Invalid file type. {file_path} must be a .csv file.")
        exit(1)
    return file_path


def validate_hour(hour):
    '''
    Validate hour passed.
    '''
    if len(hour) != 2:
        print(f"Error: Invalid hour. {hour} should be two digits, optionally with 'H' or 'h'.")
        exit(1)
    return

def get_contents(file_path):
    with file_path.open() as f:
        reader = csv.reader(f)
        contents = [line for line in reader]
    return contents

def get_contents_dict(file_path):
    with file_path.open() as f:
        reader = csv.DictReader(f)
        contents_dict = [line for line in reader]
    return contents_dict

def get_averages(lines):
    ups = []
    dns = []
    for line in lines:
        dns.append(float(line['Download']))
        ups.append(float(line['Upload']))
    return {'Down': statistics.mean(dns), "Up": statistics.mean(ups)}

def convert_timestamp(timestamp):
    date = timestamp.split('T')[0]
    time = timestamp.split('T')[1].split('.')[0]
    return (date, time)

def convert_to_Kbps(rate):
    # speedtest returns speeeds as "bits per second";
    #   convert to kilobits per second
    return round(float(rate) // 1024)

def cat(args):
    file_path = Path(args.infile)
    validate_file(file_path)
    lines = get_contents(file_path)
    # Print column headers
    l = lines[0]
    print(f"{l[0]:>10}  {l[1]:^10.10}  {l[3]:^19.19}  {l[6]:^10}  {l[7]:^10}")
    # Print recent stats.
    for l in lines[-300:]:
        print(f"{l[0]:>10}  {l[1]:10.10}  {l[3]:19.19}  {float(l[6]):>10.2f}  {float(l[7]):>10.2f}")
    exit(0)

def avg(args):
    hour = args.average[0].lower().replace('h', '')
    validate_hour(hour)
    file_path = Path(args.infile)
    validate_file(file_path)
    lines = get_contents_dict(file_path)

    i_start = None
    hour_lines = []
    for i, line in enumerate(lines):
        line_date = line['Timestamp'].split('T')[0]
        line_hour = line['Timestamp'].split('T')[1].split(':')[0]
        if line_hour == hour:
            if not i_start and ( i >= len(lines) - 500 or i == len(lines) - 1 ):
                i_start = i
            if i_start and i >= i_start:
                hour_lines.append(line)
    if hour_lines:
        if len(hour_lines) > 30:
            hour_lines = hour_lines[-30:]
        num_entries = len(hour_lines)
        avgs = get_averages(hour_lines)
        dn = convert_to_Kbps(avgs['Down'])
        up = convert_to_Kbps(avgs['Up'])
        print(f"Average bandwidth, {hour}h, last {num_entries} entries: Down: {dn} Kbps; Up: {up} Kbps")
    else:
        print(f"No data for {hour}h. Try odd-numbered hours (01, 03, 05, etc.)")
    exit(0)

def main():
    # Add arguments.
    parser = argparse.ArgumentParser(description="Slice and dice speedtest results.")
    parser.add_argument(
        "-i", "--infile", type=str, metavar='/path/to/file.csv',
        default=f"{Path(environ['HOME']) / 'speedtest.csv'}",
        help="The CSV file whose data is sliced and diced."
    )
    parser.add_argument(
        "-a", "--average", type=str, nargs=1,
        metavar='HH', default=['09H'],
        help="Calcuates the average over the last month for the given hour \'HH\'"
        )
    parser.add_argument(
        "-c", "--cat", action='store_true',
        help="Dump the contents of the CSV file."
        )

    args = parser.parse_args()
    if args.infile == None:
        args.infile = get_default_infile()
        if args.infile == None:
            print(f"Error: No file given and no file found in default locations.")
            exit(1)
    if args.cat == True:
        cat(args)
    elif args.average != None:
        avg(args)

if __name__ == '__main__':
    main()
