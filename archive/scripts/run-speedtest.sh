#!/bin/bash

# Set variables.
log="/home/acatbadmin/Partages/apps-etc/speedtest.csv"
script=$(realpath $0)
cj_line="15 */2 * * * acatbadmin $script" # 15min after every other hour

# Ensure packages are installed.
if [[ ! $(which speedtest-cli) ]]; then
    echo "Installing speedtest-cli..."
    sudo apt-get update && sudo apt-get install speedtest-cli
    if [[ $? -ne 0 ]]; then
        echo "ERROR: Installation failed. Exiting."
    fi
fi

# Ensure cronjob is enabled.
cj_speedtest="/etc/cron.d/run-speedtest"
if [[ ! -e "$cj_speedtest" || $(cat "$cj_speedtest") != "$cj_line" ]]; then
    echo "Creating cronjob..."
    sudo touch "$cj_speedtest"
    echo "$cj_line" | sudo tee "$cj_speedtest"
fi

# Run speedtest.
speedtest-cli --csv --timeout 60 | tee -a "$log"
