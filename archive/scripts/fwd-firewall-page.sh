#!/bin/bash

# ==============================================================================
# Forward firewall:443 page to server:443 for remote access.
# ==============================================================================

IP_FW=172.16.1.1
IP_SRV=172.16.1.4
PORT=443

# Enable port forwarding (does not survive reboot).
sysctl --write "net.ipv4.ip_forward=1"
sysctl --load

# Delete all chains.
iptables --flush
# Delete all rules in "nat" table.
iptables --table nat --flush
# Delete all user-defined chains.
iptables --delete-chain
# Add forwarding rules to "nat" table.
iptables -t nat -A PREROUTING -p tcp --dport $PORT -j DNAT --to-destination ${IP_FW}:${PORT}
iptables -t nat -A POSTROUTING -p tcp -d $IP_FW --dport $PORT -j SNAT --to-source $IP_SRV
