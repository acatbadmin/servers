#!/bin/bash

ADMIN='it_admin'

# Should be run with sudo.
if [[ $(id -u) != 0 ]]; then
    echo "Need to run with elevated privileges. Exiting."
    exit 1
fi

# [Lots of forgotten stuff that I did manually...]
# e.g. Add accounts in GUI: it_admin, it_assistant

# Ensure wireguard enabled and started.
echo "Ensuring that wireguard is enabled and started..."
svc="pkg-wg-quick@wgcar0.service"
if [[ -d /var/packages/Wireguard ]]; then
    if [[ -f /etc/wireguard/wgcar0.conf ]]; then
        if [[ $(systemctl is-enabled "$svc") != 'enabled' ]]; then
            wg-autostart enable wgcar0
        fi
        # Ensure that it's running properly.
        if ! ip a | grep -q wgcar0; then
            systemctl restart pkg-wg-quick@wgcar0.service
        fi
    else
        echo "WARNING: wgcar0 config not set."
    fi
else
    echo "WARNING: Wireguard package not installed."
fi

# Ensure python3 installation.
echo "Ensuring python3 installation..."
if [[ -z $(which python3) ]]; then
    synopkg install_from_server Python3.9 # DISM 7.0
    # Add new python3 to it_admin PATH ahead of other PATH items.
    if id "$ADMIN" >/dev/null 2>&1; then
        echo 'export PATH="/volume1/@appstore/Python3.9/usr/bin:$PATH"' \
            >> "/var/services/homes/$ADMIN/.profile"
    else
        echo "ERROR: No account \"$ADMIN\". Exiting."
        exit 1
    fi
fi

# Ensure python3 pip.
echo "Ensuring pip installation..."
if ! sudo --user="$ADMIN" python3 -c 'import pip' 2>/dev/null; then
    echo "Installing python3 package pip..."
    sudo --user="$ADMIN" python3 -m ensurepip
fi
# Ensure up-to-date pip.
echo "Ensuring that pip is up-to-date..."
sudo --user="$ADMIN" python3 -m pip install --upgrade pip # to get the most recent version

# Ensure python3 modules.
echo "Ensuring installation of python3 modules..."
modules=(
    'bs4'
    'packaging'
    'requests'
)
for m in "${modules[@]}"; do
    if ! sudo --user="$ADMIN" python3 -c "import $m" 2>/dev/null; then
        echo "Installing python3 package $m..."
        sudo --user="$ADMIN" python3 -m pip install "$m"
    fi
done
