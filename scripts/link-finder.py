#!/usr/bin/env python3

# ------------------------------------------------------------------------------
#   2021-01-21
#   This script is designed to be generic enough to add any app's download
#   page to it, as long as that webpage doesn't require java to generate the
#   html code. (This is what Bloom's page does, which will soon require a
#   workaround.)
#
#   The lines written to download-links have this form:
#       Paratext 8 patch|https://paratext.org/files/releases/8.0/patch_8.0.100.99.msp  # noqa: E501
# ------------------------------------------------------------------------------

import datetime
import json
import re
import requests
import shutil
import sys

from bs4 import BeautifulSoup
from pathlib import Path


# ------------------------------------------------------------------------------
# Set initial variables.
# ------------------------------------------------------------------------------
# For testing.
test = False
if len(sys.argv) > 1 and sys.argv[1] == "test":
    test = True

# The output file is where the rest of the download links are stored.
script_path = Path(__file__).absolute()
output_file = script_path.parents[1] / "data" / "download-links"
output_file.absolute()
temp = None

langtran_root = "http://63.142.243.28"

# List extenstions whose links will be scraped from given web pages.
extensions = ['exe', 'msp', 'msi', 'zip']
# Each "page" is an app name followed by its web address.
pages = {
    # This works, but with special handling since not an html file.
    # "Bloom": "https://s3.amazonaws.com/versions.bloomlibrary.org/latest.Release.json",  # noqa: E501
    # "Bloom": "https://bloomlibrary.org/page/create/downloads", # doesn't work
    #  This uses relative links...
    # "Bloom": "http://files.lingtransoft.info/Literacy_win/Bloom_easy_to_prepare_books_for_new_readers/",  # noqa: E501
    "Bloom LangTran": f"{langtran_root}/Literacy_win/Bloom_easy_to_prepare_books_for_new_readers",  # noqa: E501
    "Chrome LangTran": f"{langtran_root}/Internet_win/Google_Chrome_web_browser",  # noqa: E501
    "HearThis": "https://software.sil.org/hearthis/download",
    # As of 2023-03-17
    "LibreOffice": "https://www.libreoffice.org/download/download-libreoffice/?type=win-x86_64&lang=fr",  # noqa: E501
    # Permalink as of 2022-01-24
    # "Paratext": "https://paratext.org/download/",
    # "PTXPrint": "https://software.sil.org/ptxprint/download/",
    "PTXPrint LangTran": f"{langtran_root}/Translation_win/Paratext_and_associated_tools/PTXprint_to_format_USFM_files_for_printing",  # noqa: E501
    "Syncthing": "https://syncthing.net/downloads/",
    # "Syncthing": "https://github.com/syncthing/syncthing/releases/latest",
}


# ------------------------------------------------------------------------------
# Define functions.
# ------------------------------------------------------------------------------
def get_links(app=None, url=None):
    '''
    Scrape links from given webpages.
    '''
    links = []

    win_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)'

    headers = {}
    if app in ["HearThis"]:
        headers['User-Agent'] = win_agent

    # Get page content.
    try:
        r = requests.get(url, headers=headers, timeout=30)
    except Exception as e:
        print(repr(e))
        print(f"Error: No links passed for {app}.")
        return {app: None}

    source = None
    if app.split()[-1] == 'LangTran':
        source = 'LangTran'

    if app == "Bloom":
        # This page is just a json string on a public page.
        return {app: [(json.loads(r.content.decode())["url"])]}

    # Filter out link(s) from page.
    soup = BeautifulSoup(r.content, 'html.parser')
    all_links = soup.select('a')

    # This pattern list requires that site links be full, not relative (e.g.
    #  "files.lingtransoft.info").
    pats = [r"http.*\." + ext for ext in extensions]
    if source == 'LangTran':
        pats = [r".*\." + ext for ext in extensions]

    for item in all_links:
        link = item.get('href')
        for pat in pats:
            try:
                match = re.match(pat, link)
                ln = match.group()
                if source == 'LangTran':  # adapt to relative links
                    ln = f"{url}/{ln}"
                links.append(ln)
            except Exception:
                pass
    links = list(set(links))
    links.sort()
    return {app: links}


def filter_links(links_dict):
    '''
    Filter out unwanted links from links_dict.
    '''
    for app, links in links_dict.items():
        if test:
            print(f"Filtering out unwanted links for {app} from {links}")
        if not links:
            continue

        if app == 'Paratext':
            for link in links[:]:
                # Only keep links for v9 and newer.
                ver = link.split('/')[5]
                if ver < '9.0':
                    links.remove(link)
                # Remove links for online installer.
                type = link.split('_')[-1]
                if type == "InstallerOnline.exe":
                    links.remove(link)

        elif app == 'LibreOffice':
            # Get version numbers.
            versions = {}
            for link in links[:]:
                ver = link.split('/')[-1].split('_')[1]
                try:
                    versions[ver].append(link)
                except KeyError:
                    versions[ver] = [link]

            # Define stable version to keep.
            vers = list(versions.keys())
            vers.sort()
            ver_stable = vers[0]

            # Remove links of risky version(s).
            for link in links[:]:
                ver = link.split('/')[-1].split('_')[1]
                if ver != ver_stable:
                    links.remove(link)

            # Modify the app link in order to skip the donate page.
            base_uri = ''
            app_link = ''
            checking = True
            while checking:
                for i, link in enumerate(links[:]):
                    parts = link.split('/')
                    uri = parts[:-1]
                    filename = parts[-1]
                    fileparts = filename.split('_')
                    if 'helppack' in fileparts:
                        # This includes the main download path.
                        base_uri = uri
                    else:
                        # This includes the donate path.
                        app_link = '/'.join([*base_uri, filename])
                    if base_uri and app_link:
                        links[i] = app_link
                        checking = False

        elif app == 'PTXPrint':
            # Keep newest version.
            links.sort()  # don't assume they're in any particular order
            while len(links) > 1:
                links.pop(0)

        elif app == 'Syncthing':
            if test:
                print(links)
            # Only keep Windows 64-bit version.
            for link in links[:]:
                os_arch = link.split('-')[1:3]
                if os_arch != ['windows', 'amd64']:
                    links.remove(link)

        elif app == 'HearThis':
            links.sort()
            while len(links) > 1:
                links.pop(0)

        else:
            # For general apps, just keep all found links.
            pass

    return links_dict


def create_link_lines(links_dict):
    '''
    Generate each link line for download-links file.
    '''
    # Each line looks like this:
    # Paratext 8 patch|https://paratext.org/files/releases/8.0/patch_8.0.100.99.msp  # noqa: E501
    # "App Name" ["App Version"] "App Part"|"link"
    lines = []
    for app, links in links_dict.items():
        if not links:
            print(f"No links for {app}. Skipping.")
            continue
        name = app
        for link in links:
            filename = link.split('/')[-1]
            if name == "Paratext":
                # Get PT version and app/patch from filename.
                ver = filename.split('_')[1]
                ext = filename.split('.')[-1]
                if ext == 'msp':
                    part = "patch"
                else:
                    part = "app"
                lines.append(f"{name} {ver} {part}|{link}\n")
            elif name == "LibreOffice":
                # Get app/help from filename.
                if filename.split('_')[-2] == 'helppack':
                    part = "help"
                else:
                    part = "app"
                lines.append(f"{name} {part}|{link}\n")
            else:
                # No filtering applied.
                lines.append(f"{name}|{link}\n")
    return lines


def create_timestamp_line():
    # Returns a string like this:
    # List updated|2021-01-21 14:14:48.187465
    return f"List updated|# {datetime.datetime.now()}\n"


def edit_file(f, lines_added):
    '''
    Update the download-links file with the new links.
    '''
    special_apps = [line.split('|')[0].split()[0] for line in lines_added]
    special_apps = list(set(special_apps))
    special_apps.insert(0, "List")

    f = Path(f)
    lines_new = []
    if not test:
        if f.exists():
            # Make a backup.
            suffix = ".bak"
            backup = f.with_suffix(suffix)
            shutil.copyfile(f, backup)
        else:
            print(f"Error: {f} is missing. Restore from backup and try again.")
            exit(1)

    # Get file contents.
    with open(f, 'r') as f:
        lines_orig = f.readlines()

    for line in lines_orig:
        name = line.split('|')[0].split()[0]
        if name not in special_apps:
            lines_new.append(line)

    lines_new.extend(lines_added)
    lines_new.insert(0, create_timestamp_line())
    if test:
        print(f"special apps: {' '.join(special_apps)}")
        for line in lines_new:
            print(line, end='')
        exit(9)
    with open(f, 'w') as f:
        for line in lines_new:
            f.write(line)


def main():
    # Get all links listed on given webpages.
    links_dict = {}
    for app, url in pages.items():
        if test:
            print(f"Getting links for {app} from {url}...")
        links_dict.update(get_links(app=app, url=url))

    # Filter out unwanted links.
    f_links_dict = filter_links(links_dict)

    # Create lines for download-links file.
    lines = create_link_lines(f_links_dict)

    # Add updated lines to download-links file.
    edit_file(output_file, lines)


if __name__ == '__main__':
    main()
