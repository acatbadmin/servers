#!/bin/bash


script="$0"
script_name="${script##*/}"
script_dir="${script%/*}"
repo_dir="${script_dir%/*}"
data_dir="${repo_dir}/data"
download_dir="${HOME}/Partages/apps-etc" # ubuntu
log_dir="${HOME}/log"
system="ubuntu"
if [[ -f /etc.defaults/VERSION ]]; then
    # Synology server.
    system="synology"
    download_dir="/volume1/apps-etc"
    log_dir="/volume1/it-dept/log"
fi
log="${log_dir}/${script_name%.*}.log"
input_file="${log_dir}/${script_name%.*}.links"
# infile="$download_dir/download-links"
infile="${data_dir}/download-links"
attempts=1
if [[ $1 -gt 0 ]]; then
    attempts=$1
fi


# ------------------------------------------------------------------------------
# Define functions.
# ------------------------------------------------------------------------------
net_check () {
    curl ip.me >/dev/null 2>&1
    return $?
}

make_input_file () {
    cat $infile | cut -d'|' -f2 > "$input_file"
}

get_downloads () {
    date >> "$log"
    if [[ "$system" == 'ubuntu' ]]; then
        aria2c --log="$log" --input-file="$input_file" --continue \
            --dir="$download_dir" --conditional-get --check-integrity \
            --remote-time --log-level=notice --allow-overwrite \
            --follow-metalink=mem --max-connection-per-server=10

        #--log					The FILE name of the log file
        #--dir					The directory to store the file
        #--check-integrity		Check file integrity
        #--continue				Continue downloading a partial file
        #--input-file			Downloads URIs found in FILE
        #--remote-time			Apply remote file time to local file
        #--conditional-get		Download only when remote file is newer
        #--summary-interval		Set output progress interval in seconds
        #--log-level			debug; info; notice; warn; error
        #--allow-overwrite		Restart if control file doesn't exist
        #--follow-metalink		true (downloads files & ML file); false
                                # (downloads ML file only;
                                # mem (keeps ML in memory but does not
                                # save it)
        #--max-download-limit	B/s by default, or use K or M (ex: 20K)
        #--max-connection-per-server	default is 1
    elif [[ "$system" == 'synology' ]]; then
        # TODO: Add file integrity check.
        #   This will probably involve downloading one file at a time, while
        #   first getting the header to see what the filesize should be. Then,
        #   when wget exits, comparing the downloaded file with this expected
        #   filesize. If the sizes are different, delete the downloaded file.
        wget --append-output="$log" --input-file="$input_file" \
            --directory-prefix="$download_dir" --timestamping --no-verbose \
            --trust-server-names --no-hsts
        # Hacky file permissions fix.
    else
        echo "OS not supported" | tee -a "$log"
        return 1
    fi
}

# script arg. $1 is the number of times to attempt the downloads.
start_download_attempts () {
    status=
    count=0
    while [[ $status != 0 ]] && [[ $count -lt $1 ]]; do
        get_downloads
        status=$?
        if [[ $count -gt 1 ]]; then
            echo "End of attempt $count of $1" | tee -a "$log"
        fi
        ((count++))
    done
}

# ------------------------------------------------------------------------------
# Main processing.
# ------------------------------------------------------------------------------
# Ensure that .profile is sourced to set env.
if [[ -z "$PROFILE_SOURCED" ]]; then
    source "$HOME/.profile"
fi

# Initial log line.
# Ensure log_dir exists.
mkdir -p "$log_dir"
touch "$log"
echo >> "$log"
echo "Session start: $(date)" | tee -a "$log"
echo "System: $system" | tee -a "$log"
echo "Log file: $log"

# Check for an internet connection.
echo "Verifying internet connection..."
if ! net_check; then
    echo "No internet connection. Exiting." | tee -a "$log"
    exit 1
fi

# Update non-immutable links.
echo "Updating download links..." | tee -a "$log"
"${script_dir}/link-finder.py"

# Start downloading. The digit is how many times to attempt the downloads.
echo "Starting downloads..." | tee -a "$log"
make_input_file
start_download_attempts $attempts

# Fix syncthing file name(s).
echo "Fixing syncthing filename..." | tee -a "$log"
find "$download_dir" -regex '^.*\-Amz\-.*' -type f -exec "${script_dir}/fix-syncthing-filename.sh" {} \;

# Archive old file versions.
echo "Archiving old installers..." | tee -a "$log"
"${script_dir}/keep-newest-versions.py" "$download_dir"
echo "Session end: $(date)"

exit 0
