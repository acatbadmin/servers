#!/bin/bash

# Get the hostname and MAC address of a machine on the local network with the given IP address.

# Accept multiple IPs as args.
if [[ $1 == '-a' ]]; then
    read -rp "This could take a really long time for 500+ addresses. Continue? [y/N] " ans
    if [[ "${ans,,}" != 'y' ]]; then
        exit 1
    fi
    ip4_16="172.16"
    ip_list=
    for d in {0..1}; do
        for n in {1..254}; do
            ip_list+=" ${ip4_16}.${d}.${n}"
        done
    done
elif [[ "$*" ]]; then
    ip_list="$*"
else
    echo "Need at least 1 IP address as args."
    exit 1
fi

# Gather info from given IPs.
for ip_addr in $ip_list; do
    # Use nmblookup.
    hostnames=$(nmblookup -A "$ip_addr" | head -n4 | tail -n3 | cut -f2 | cut -d' ' -f1)
    if [[ "${hostnames:0:7}" == "Looking" ]]; then
        # No response; exit silently.
        # exit 1
        continue
    fi
    mac_addr=$(nmblookup -A "$ip_addr" | grep MAC | cut -d'=' -f2 | tr '-' ':')

    # Output results.
    echo $ip_addr
    echo $hostnames
    echo $mac_addr
    echo
done
