#!/usr/bin/env bash

scripts_dir="$(realpath "$(dirname "$0")")"
if [[ $1 == '-h' || $1 == '--help' ]]; then
    echo "usage: $0 /PATH/TO/LINKS_FILE"
    exit 0
fi
links_file="$1"

# Update LibreOffice version number.
lo_ver="$("${scripts_dir}/get-libreoffice-latest-version-number.py")"
vpat='[0-9]+\.[0-9]+\.[0-9]+'
sed -ri \
    "s|^(.*/libreoffice/stable/)(${vpat})(/win/x86_64/LibreOffice_)\2(.*)|\1${lo_ver}\3${lo_ver}\4|" \
    "$links_file"
