#!/usr/bin/env python3

# Keep newest X versions of similarly-named files in a given directory. (defaults to "2")
# Older versions get moved to subfolder. (defaults to "./archive")

import argparse
import re
import tempfile

from packaging import version
from pathlib import Path


def validate_target_folder(folder_string):
    folder_path = Path(folder_string).resolve()
    if not folder_path.is_dir():
        print(f"ERROR: \"{folder_string}\" is not a valid folder")
        exit(1)
    return folder_path

def validate_archive_folder(folder_string):
    # Need ensure that archive folder exists or can be created, and that user
    #   has write permissions to it.
    folder_path = Path(folder_string).resolve()
    # See if it exists or can be created.
    try:
        folder_path.mkdir(parents=True, exist_ok=True)
    except:
        print(f"ERROR: Can't create \"{folder_string}\"")
        exit(1)
    # See if user can write to it.
    try:
        with tempfile.TemporaryFile(dir=folder_path, mode='w+t') as tf:
            tf.write('test')
    except PermissionError:
        print(f"ERROR: Can't copy files to \"{folder_string}\"")
        try:
            folder_path.rmdir() # fails if not empty
        except:
            pass
        exit(1)
    return folder_path

def get_version_string(filestem):
    # Examples of filestems:
    #   - Skype-8.74.0.152
    #   - syncthing-windows-amd64-v1.16.1
    #   - linux-generic-hwe-20.04_5.11.0.43.47~20.04.21_amd64
    # The separator is the first character that's not part of the file name.
    # It could be: <space>, <period>, <underscore>, or <dash>
    # Find r where the initial char is followed by valid version chars and ending with 0-9.
    sep_chars = ' _\-\.'
    version_chars = 'mpv0-9\.\+\-\~'
    final_chars = '0-9'
    version_pat = '[' + sep_chars + ']{1}[' + version_chars + ']+[' + final_chars + ']'
    r = re.findall(version_pat, filestem, flags=re.IGNORECASE)
    # Get last item in results list.
    version_string = r[-1] if r else None
    return version_string

def get_full_filename(filestem, version_string):
    return filestem.replace(version_string, '') if version_string else None

def parse_filename(filepath):
    version_string = get_version_string(filepath.stem)
    version = version_string[1:] if version_string else None # remove leading character
    name = get_full_filename(filepath.stem, version_string)
    return name, version

def parse_file_list(file_list):
    filetypes = [
        '.deb',
        '.exe',
        '.msi',
        '.msp',
        '.zip',
    ]
    file_data = {}
    for filepath in file_list:
        # Skip folders and unsupported filetypes.
        if filepath.is_dir() or not filepath.suffix in filetypes:
            continue
        name, ver = parse_filename(filepath)
        if not name and not ver:
            # Skip filepath: no recognizable version info.
            print(f"INFO: Unknown version; skipping: {filepath}")
            continue
        if not file_data.get(name):
            file_data[name] = [ver]
        else:
            file_data[name].append(ver)
    return file_data

def sort_versions(file_data):
    sorted_data = file_data.copy()
    for name, vers in sorted_data.items():
        sorted_vers = []
        for ver in vers:
            sorted_vers = insert_item(ver, sorted_vers)
        sorted_data[name] = sorted_vers
    return sorted_data

def insert_item(item, items):
    # Insert item into version-sorted location within list of items.
    new_items = items.copy()
    if new_items:
        try:
            new_items.remove(item)
        except ValueError:
            pass
        for i, v in enumerate(items):
            if version.parse(item) > version.parse(v):
                new_items.insert(i, item)
                break
        if item not in new_items:
            new_items.append(item)
    else:
        new_items.append(item)
    return new_items

def trim_file_list(to_keep, file_data):
    # Trim list of versions to keep.
    trimmed_data = file_data.copy()
    for k, v in trimmed_data.items():
        trimmed_data[k] = v[:to_keep]
    return trimmed_data

def setup_args():
    desc = "Keep N newest versions (default=2) of software package files; move others to archive folder (default=TARGET_DIR/archive)."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "target_dir", metavar='TARGET_DIR', type=str, nargs=1,
        help="target folder containing software packages"
    )
    parser.add_argument(
        "-i", "--ask", action='store_true',
        help="ask for confirmation before moving each file"
        )
    parser.add_argument(
        "-d", "--dest", type=str, nargs=1,
        metavar='DEST', default="DEFAULT",
        help="destination folder for removed package files"
        )
    parser.add_argument(
        "-k", "--keep", type=int, metavar='N', default=2,
        help="number of versions of each package to keep"
    )
    return parser.parse_args()

def move_old_files(file_list, kept_data, dest, confirm):
    dest.mkdir(exist_ok=True) # ensure dest exists
    for file in file_list:
        name, ver = parse_filename(file)
        if kept_data.get(name) and ver not in kept_data.get(name):
            if confirm:
                ans = input(f"Move \"{file.name}\" to {dest}? [Y/n]: ")
                if ans and ans.lower() != 'y':
                    continue
            file.replace(dest / file.name)

def main():
    ## Set global variables.
    args = setup_args()
    # Set folder where installer files will be compared and culled.
    target_dir = validate_target_folder(args.target_dir[0])
    # Set archive folder for culled files.
    if args.dest == 'DEFAULT':
        archive_dir = target_dir / 'archive'
    else:
        archive_dir = validate_archive_folder(args.dest[0])
    # Set number of package versions of each file to keep.
    to_keep = args.keep if args.keep else 2
    # Set confirmation variable.
    ask = args.ask

    ## Run program.
    # List files in target_dir.
    files = [f for f in target_dir.iterdir()]
    # Parse file list into dictionary.
    full_data = parse_file_list(files)
    # Sort dictionary version numbers highest to lowest.
    sorted_data = sort_versions(full_data)
    # Remove oldest versions from dictionary.
    kept_data = trim_file_list(to_keep, sorted_data)
    # Move unkept files to archive folder.
    move_old_files(files, kept_data, archive_dir, ask)
    exit()


if __name__ == '__main__':
    main()
