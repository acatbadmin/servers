#!/bin/bash

# Set variables.
LANG=C
script=$(realpath "$0")
infile=$(realpath "$1")
share_name="$2"

script_name="${script##*/}"
script_dir="${script%/*}"
repo_dir="${script_dir%/*}"
data_dir="${repo_dir}/data"
system="ubuntu"
download_dir="${HOME}/Partages/${share_name}" # ubuntu
log_dir="${HOME}/log"
if [[ -f /etc.defaults/VERSION ]]; then
    # Synology server.
    system="synology"
    download_dir="/volume1/${share_name}"
    log_dir="/volume1/it-dept/log"
fi
log="${log_dir}/${script_name%.*}.log"
input_file="${infile}.tmp"

# Define functions.
quit() {
    rm -f "$input_file"
    echo | tee -a "$log"
    exit $1
}

sanitize_infile() {
    # sed -r 's@^([^#]*|)(.*)$@\2@' "$infile" > "$input_file" 2>&1 | tee -a "$log"
    grep -v '^#' "$infile" | awk -F'|' '{print $2}' > "$input_file"
    return $?
}

get_downloads() {
    if [[ "$system" == 'ubuntu' ]]; then
        aria2c --log="$log" --input-file="$input_file" --continue \
            --dir="$download_dir" --conditional-get --check-integrity \
            --remote-time --log-level=notice --allow-overwrite \
            --follow-metalink=mem --max-connection-per-server=10

        #--log					The FILE name of the log file
        #--dir					The directory to store the file
        #--check-integrity		Check file integrity
        #--continue				Continue downloading a partial file
        #--input-file			Downloads URIs found in FILE
        #--remote-time			Apply remote file time to local file
        #--conditional-get		Download only when remote file is newer
        #--summary-interval		Set output progress interval in seconds
        #--log-level			debug; info; notice; warn; error
        #--allow-overwrite		Restart if control file doesn't exist
        #--follow-metalink		true (downloads files & ML file); false
                                # (downloads ML file only;
                                # mem (keeps ML in memory but does not
                                # save it)
        #--max-download-limit	B/s by default, or use K or M (ex: 20K)
        #--max-connection-per-server	default is 1
    elif [[ "$system" == 'synology' ]]; then
        # TODO: Add file integrity check.
        #   This will probably involve downloading one file at a time, while
        #   first getting the header to see what the filesize should be. Then,
        #   when wget exits, comparing the downloaded file with this expected
        #   filesize. If the sizes are different, delete the downloaded file.
        wget --append-output="$log" --input-file="$input_file" \
            --directory-prefix="$download_dir" --timestamping --no-verbose \
            --trust-server-names --no-hsts
    else
        echo "Error: OS not supported" | tee -a "$log"
        return 1
    fi
}


# Begin processing.
date | tee -a "$log"

# Check variables.
if [[ -z "$infile" ]]; then
    echo "Error: No links file given." | tee -a "$log"
    quit 1
fi
if [[ -z "$share_name" ]]; then
    echo "Error: No destination folder name given." | tee -a "$log"
    quit 1
fi

# Check input and output files.
if [[ ! -f "$infile" ]]; then
    echo "Error: Infile does not exist: $infile" | tee -a "$log"
    quit 1
fi
if [[ ! -d "$download_dir" ]]; then
    echo "Error: Destination folder does not exist: $download_dir" | tee -a "$log"
    quit 1
fi

# Sanitize infile.
sanitize_infile
if [[ -z $(cat "$input_file") ]]; then
    echo "Error: No valid links in $infile" | tee -a "$log"
    quit 1
fi

# Log download links.
echo "Links to download:" | tee -a "$log"
cat -n "$input_file" | tee -a "$log"

# Start downloading.
get_downloads
quit $?
