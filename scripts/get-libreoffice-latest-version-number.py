#!/usr/bin/env python3

import re
import requests
from bs4 import BeautifulSoup


def match_version(item):
    pat = re.compile(r'(\d+\.\d+\.\d+)/')
    v = re.match(pat, item)
    if v:
        return v[1]
    else:
        return


url = 'http://download.documentfoundation.org/libreoffice/stable/'
with requests.get(url) as r:
    soup = BeautifulSoup(r.content, 'html.parser')
refs = [a.get('href') for a in soup.select('a')]
versions = [v for v in map(match_version, refs) if v]
print(versions[-1])
