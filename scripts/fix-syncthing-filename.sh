#!/bin/bash

file=$(realpath "$1")
dir=$(dirname "$file")
filetype=$(file --mime-type "$file" | awk -F: '{print $2}')
if [[ "$filetype" == ' application/zip' ]]; then
    # ZIP file. Is it syncthing?
    if [[ "${file##*.}" != 'zip' ]]; then
        mv "$file"{,.zip} # rename to ZIP format if extension is missing
        file="${file}.zip"
    fi
    first_file=$(zip -sf "${file}" | head -n2 | tail -n1 | sed 's/^\s*//')
    first_file=$(basename "$first_file") # remove trailing slash
    if [[ "$first_file" =~ ^syncthing ]]; then
        # This is a syncthing zip file; rename the zip file.
        mv "$file" "${dir}/${first_file}.zip"
    fi
fi
