#!/bin/bash

# USB drive needs to be ejected after backup is run so that it can be taken
#   offsite by a non-admin user who has physical access to the Synology. This
#   means that a script is required to re-mount the drive before each backup
#   attempt.

# Get correct USB address.
usb_addr=$(lsusb -e | grep External | awk '{print $1}' | tr -d '|' | tr -d '_')
# USB address hard-coded for now; should match any device in front port.
usb_addr=$(lsusb -e | grep '2\-2' | awk '{print $1}' | tr -d '|' | tr -d '_')
if [[ -z "$usb_addr" ]]; then
    echo "USB drive not attached. Exiting."
    exit 0
fi

# Ensure USB drive is already ejected.
if [[ $(mount | grep volumeUSB1) ]]; then
    echo "USB drive still mounted; nothing to do. Exiting."
    exit 0
fi

# Re-bind the USB drive.
echo "$usb_addr" > /sys/bus/usb/drivers/usb/unbind
sleep 10
echo "$usb_addr" > /sys/bus/usb/drivers/usb/bind
