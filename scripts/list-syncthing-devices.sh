#!/bin/bash

read -p "Enter path to syncthing's config.xml file: " CONFIG_XML

# Output device names and IDs, separated by '|'
xmlstarlet select -t -m /configuration/device -v "concat(@name,'|',@id)" -n "$CONFIG_XML" | sort
