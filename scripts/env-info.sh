#!/bin/bash

# Print env info for synology script troubleshooting.

echo "Ensuring that ~/.profile is sourced..."
if [[ -z $PROFILE_SOURCED ]]; then
    source "${HOME}/.profile"
fi

echo "ENV:"
printenv | sort
echo
echo "Pyhon3 requests test:"
python3 -c 'import requests; r = requests.get("https://ip.me"); print(r.content.decode())'
