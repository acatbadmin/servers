#!/bin/bash

# ==============================================================================
# - This script is used to install and configure crashplan:
#	- Ensure dependencies are met
#	- Ensure proper configuration
#
# ==============================================================================
SCRIPT="${0}"
PARENT=$(dirname "${0}")
CP_VERSION="7.0.3_1525200006703_55"
CP_VERSION=""


# ------------------------------------------------------------------------------
# Ensure elevated privileges.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}"
    exit $?
fi

cp_installers="https://m1.bk247.org:4285/client/installers/"
cp_installers="https://console.us.code42.com/login/#/login"
if [[ ! $CP_VERSION ]]; then
    echo "Veuillez taper le numéro de la version actuellement disponible à $cp_installers."
    read -p "Version : " CP_VERSION
fi


# ------------------------------------------------------------------------------
# Install & configure CrashPlan.
# ------------------------------------------------------------------------------
# Installation.
cphome='/usr/local/crashplan'
cpconfig="${cphome}/conf/my.service.xml"
cpsvc_name='crashplan.service'
cpsvc_file="/etc/systemd/system/${cpsvc_name}"
cpsrc="${PARENT}/data/crashplan"

# Verify installation.
if [[ ! $(which CrashPlanDesktop) ]]; then
    # Need to install crashplan manually: It's not in the Ubuntu APT archives,
    #   nor is there a universal public download link, so installer archive
    #   needs to be downloaded manually and put in the git repo. Alternatively,
    #   the version number can be updated here in the script and it will find and
    #   download the installer archive.

    # Install (undocumented) dependencies.
    apt-get install --yes libnss3 libxss1
    # Version 8 dependencies: libgconf-2-4 libXScrnSaver net-tools libnss3
    # https://support.code42.com/CrashPlan/6/Get_started/Code42_app_system_requirements

    temp_dir=$(mktemp --tmpdir --directory "crashplan-install.XXX")
    archive_filename="Code42_${CP_VERSION}_Linux.tgz"
    archive_file="${cpsrc}/${archive_filename}"
    link="https://m1.bk247.org:4285/client/installers/${archive_filename}"
    inst_dirname="crashplan-install"
    inst_dir="${temp_dir}/${inst_dirname}"
    if [[ ! -e $archive_file ]]; then
        echo "La version $CP_VERSION sera téléchargée."
        read -p "Si une autre version est préférée, tappez le numéro ici : " version
        echo "Téléchargement du fichier ${link}..."
        aria2c --continue --max-connection-per-server=5 --dir="$temp_dir" "$link"
    fi

    # Decompress the archive to a temp directory.
    if [[ ! -d $inst_dir ]]; then
        echo "Décompression du ficher ${archive_file}."
        tar --gunzip --extract --keep-old-files \
            --file="$archive_file" --directory="$temp_dir"
    fi

    # Install the app.
    if [[ -e $inst_dir ]]; then
        echo "Installation de CrashPlan avec ${inst_dir}/install.sh --quiet"
        "${inst_dir}/install.sh" --quiet # use default options
    fi
fi

# Kill auto-run process.
cppid=$(pgrep CrashPlan)
if [[ $cppid ]]; then
    kill $cppid
fi

# Verify configuration.
#if [[ ! -e $cpconfig ]]; then
if [[ ! $(grep --no-messages 'authority address="m1.bk247.org:4282"' $cpconfig) ]]; then
    echo "Copie du fichier de configuration à ${cpconfig}"
    mkdir -p "${cphome}/conf"
    cp "${cpsrc}/my.service.xml" "${cpconfig}"
fi

# Make sure service is installed, active, and enabled.
if [[ ! -e $cpsvc_file ]]; then
    echo "Copie du ficher de service à $cpsvc_file"
    cp "${cpsrc}/crashplan.service" "$cpsvc_file"
    systemctl daemon-reload
fi

if [[ ! $(systemctl is-active $cpsvc_name --quiet) ]]; then
    echo "Redémarrage du service $cpsvc_name."
    systemctl start "$cpsvc_name"
fi

# TODO: Do I want this if the service is stopped and started by a cronjob?
#if [[ ! $(systemctl is-enabled $cpsvc_name --quiet) ]]; then
#    systemctl enable "$cpsvc_name"
#fi

# Start and stop crashplan at certain times.
cp_cron="/etc/cron.d/crashplan-jobs"
if [[ ! -e $cp_cron ]]; then
    echo "Installation de $cp_cron."
    cp "${cpsrc}/crashplan-jobs" "$cp_cron"
fi

# Need GUI access to sign in to the account.
cpdesktop_file="/usr/share/applications/crashplan.desktop"
if [[ ! -e $cpdesktop_file ]]; then
    echo "Installation de $cpdesktop_file."
    cp "${cpsrc}/crashplan.desktop" "$cpdesktop_file"
fi
