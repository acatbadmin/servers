### Test scripts in VM
- [x] 0-setup-squid-deb-proxy.sh
- [x] 0-setup-system-server-base.sh
- [x] 1-setup-firewall-forwarding.sh
- [x] 1-setup-wireguard-192-168-2-4.sh
- [x] 2-setup-backups-crashplan.sh
- [ ] 2-setup-backups-syncthing.sh
- [x] 2-setup-unifi-controller.sh
- [ ] 3-setup-smb-service.sh
- [ ] 3-setup-vm-wasta-2004.sh

### Test scripts on real servacatba
- [x] 0-setup-squid-deb-proxy.sh
- [ ] 0-setup-system-server-base.sh
- [x] 1-setup-firewall-forwarding.sh
- [ ] 1-setup-wireguard-192-168-2-4.sh
- [ ] 2-setup-backups-crashplan.sh
- [ ] 2-setup-backups-syncthing.sh
- [ ] 2-setup-unifi-controller.sh
- [ ] 3-setup-smb-service.sh
- [ ] 3-setup-vm-wasta-2004.sh

### Other ideas
- Add rsyncd to servacatba?

```bash
$ cat /persistent/configs/rsyncd.conf
uid=$UID
gid=$GID
hosts allow = 192.168.2.0/24 172.16.1.0/24
reverse lookup = no
list = no
log file = /var/log/rsyncd/rsyncd.log
transfer logging = yes

[archives]
read only = no
incoming chmod = g+rwX
path = /persistent/data/publicData/Archives/./
hosts allow = $hostname, 25.0.0.0/8
```

See also:
```bash
$ man rsyncd.conf
```
