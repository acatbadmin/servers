#!/bin/bash

# ==============================================================================
# Configure system to automatically download updates for frequently-used apps.
# ==============================================================================

# ------------------------------------------------------------------------------
# Initial setup.
# ------------------------------------------------------------------------------
# Ensure running as root.
if [[ $(id -u) -ne 0 ]]; then
    echo "ERROR: Need to run as sudo."
    exit 1
fi

# Set global variables.
user="acatbadmin" # the one who will "own" the download script when run
release=$(cat /etc/os-release | grep VERSION_CODENAME | awk -F= '{print $2}')
dnsutils=dnsutils
if [[ "$release" == 'bionic' ]]; then
    dnsutils=dnsutils
elif [[ "$release" == 'focal' ]]; then
    dnsutils=bind9-dnsutils
fi
debdeps=(
    aria2
    $dnsutils
)

# ------------------------------------------------------------------------------
# Check that dependencies are installed.
# ------------------------------------------------------------------------------
# Gather list of uninstalled deps.
installs=()
for dep in "${debdeps[@]}"; do
    if [[ ! $(dpkg -l | grep -E "^ii.*\W$dep\W") ]]; then
        echo "Adding to installs list: $dep"
        installs+=( "$dep" )
    fi
done
# Install deps in one run.
if [[ -n "${installs[@]}" ]]; then
    echo "Installing: ${installs[@]}"
    apt-get --yes update
	apt-get --yes install "${installs[@]}"
fi

# ------------------------------------------------------------------------------
# Ensure that cronjob is configured.
# ------------------------------------------------------------------------------
app_dl_job='/etc/cron.d/download-frequent-apps'
script_path=$(realpath ./scripts/download-frequent-apps.sh)
if [[ -x "$script_path" ]]; then
    echo "Adding/Updating cronjob: $app_dl_job"
    # Run job once per day @15h30.
    cat >"$app_dl_job" << MULTILINE
30 15 * * * "$user" "$script_path"
MULTILINE
fi
