#!/bin/bash

# ==============================================================================
# - This script should be used to create Shares and Share permissions
# - It assumes a standard Wasta 20.04 system as a starting point.
# - The configured shares are:
#   - formations
#   - wasta
#   - windows
#
# The general script outline is:
#	- Ensure dependencies are met
#	- Allow Samba in system firewall (esp. Ubuntu derivatives)
#	- Create (or modify) non-standard groups
#	- Create (or modify) users

#	- Create (or modify) folders
#	- Create (or modify) smb.conf to match
#
# ==============================================================================


# ------------------------------------------------------------------------------
# Global variables.
# ------------------------------------------------------------------------------
SCRIPT="${0}"
PARENT_DIR=$(dirname $(realpath $0))
LXD_DATA_DIR="${PARENT_DIR}/data/lxd"
RSYNCD_DATA_DIR="${PARENT_DIR}/data/rsyncd"
PROFILE='sambashare'
LXD_STORAGE='sambashare'
NAME='servacatba'
SHARE_BASE=/var/share
SUBNET="172.16.1.0/24"
ADMIN_USER="sambashare"
cont_home="/home/${ADMIN_USER}"


# Ensure installation of snapd.
if [[ ! $(which snap) ]]; then
    echo "Updating and installing snap & zfs-utils packages..."
    sudo apt-get --yes update
    sudo apt-get --yes upgrade
    sudo apt-get --yes install snap zfsutils-linux
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install snap & zfs-utils. Exiting."
        exit 1
    fi
fi

# Ensure installation of lxd.
if [[ ! $(which lxd) ]]; then
    echo "Installing lxd..."
    sudo snap install lxd
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install lxd snap. Exiting."
        exit 1
    fi
    # Add user to lxd group.
    sudo usermod -a -G lxd $(whoami)
fi

# Ensure initialization of lxd (including storage bin).
if [[ ! $(lxc storage list | grep $LXD_STORAGE) ]]; then
    echo "Initializing lxd..."
    preseed_file="${LXD_DATA_DIR}/sambashare-preseed.yaml"
    cat "$preseed_file" | lxd init --preseed
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to init lxd. Exiting."
        exit 1
    fi
fi

# Ensure that nano is the default editor (helps with lxc).
prof_line="export EDITOR=nano"
if [[ ! $(grep "$prof_line" ~/.profile) ]]; then
    echo "$prof_line" >> ~/.profile
    source ~/.profile
fi

# Ensure sambashare container.
if [[ ! $(lxc list | grep " $NAME ") ]]; then
    echo "Installing $NAME container..."
    lxc launch ubuntu:20.04 "$NAME" --verbose --profile="$PROFILE"
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to create $NAME container. Exiting."
        exit 1
    fi
    # Create post-launch snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "1-container-created"
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 5
fi

# Ensure container is running.
if [[ $(lxc info "$NAME" | grep Status | awk '{print $2}') != 'Running' ]]; then
    echo "Starting $NAME container..."
    lxc start "$NAME"
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to start $NAME containter. Exiting."
        exit 1
    fi
    # Show file system usage.
    lxc exec "$NAME" -- df -h /
    echo
    # Brute force delay to make sure container is reponsive to later commands.
    sleep 15
fi

# Ensure static IP.
yaml_name="99-acatba-static-ip-4.yaml"
yaml_exists=$(lxc exec "$NAME" -- find /etc/netplan -name "$yaml_name")
if [[ ! $yaml_exists ]]; then
    echo "Configuring netplan for static IP..."
    lxc file push "${LXD_DATA_DIR}/${yaml_name}" "${NAME}/etc/netplan/${yaml_name}"
    lxc exec "$NAME" -- netplan apply
    echo "Updated network details:"
    lxc exec "$NAME" -- ip -br a
    echo
fi

# Ensure user on container.
if [[ ! $(lxc exec "$NAME" -- grep $ADMIN_USER /etc/passwd) ]]; then
    # Create admin user.
    echo "Creating $ADMIN_USER user..."
    lxc exec "$NAME" -- adduser --gecos 'Samba Admin,,,' --disabled-login --uid 1998 $ADMIN_USER
    lxc exec "$NAME" -- adduser $ADMIN_USER adm
    lxc exec "$NAME" -- adduser $ADMIN_USER sudo
    # Set 1-time password if not already set (status=P if set).
    status=$(lxc exec "$NAME" -- passwd --status $ADMIN_USER | awk '{print $2}')
    if [[ $status != P ]]; then
        echo -e 'password\npassword' | lxc exec "$NAME" -- passwd $ADMIN_USER
    fi
    # Create user-config snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "2-user-created"
fi

# Ensure that samba packages are installed.
pkgs="nano samba samba-vfs-modules" # system-config-samba?
if [[ ! $(lxc exec "$NAME" -- which pdbedit) ]]; then
	lxc exec "$NAME" -- apt-get --yes update
	lxc exec "$NAME" -- apt-get --yes install "$pkgs"
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install $pkg. Exiting."
        exit 1
    fi
    # Create snapshot.
    echo "Creating snapshot..."
    lxc snapshot "$NAME" "3-samba-packages-installed"
fi


# ------------------------------------------------------------------------------
# Define samba setup functions.
# ------------------------------------------------------------------------------
newFolder() {
    folder="${1}"
    owner="${2}"
    group="${3}"
    mode="${4}"
	# echo
	echo "-- $folder --"
	lxc exec "$NAME" -- mkdir -p $SHARE_BASE/$folder
	lxc exec "$NAME" -- chown $owner:$group $SHARE_BASE/$folder
	lxc exec "$NAME" -- chmod $mode $SHARE_BASE/$folder
	# echo "- Dossier confirmé :"
	# echo "    $folder, propriétaire $owner, groupe $group, permissions $mode"
}

newGroup() {
    gid="${1}"
    gname="${2}"
	# echo
	echo "-- $gname --"
	lxc exec "$NAME" -- addgroup --gid $gid $gname
}

newUser() {
    # The following variables assumed to be set before calling this function:
	# $userid, $username, $passwd
    userid="${1}"
    username="${2}"
    passwd="${3}"
	# echo

	# Delete and create user (unix account): (delete to make sure uid correct)
	# true returned if UID not found so won't error
	echo "-- $username --"
    curr_uid=$(lxc exec "$NAME" -- id -u $username || true)
    if [[ "$curr_uid" ]] && ! [[ $curr_uid == $userid ]]; then
        echo
		echo "	- Supprimation de l'utilisateur pour refair uid : $username"
		read -p "		- uid actuel : $curr_uid; nouvel uid : $userid"
        lxc exec "$NAME" -- deluser $username
    fi

    # # adduser: don't create home directory, except for $ADMIN_USER
	# echo "Confirmation d'utilisateur du système..."
	# if [[ $username == $ADMIN_USER ]]; then
	# 	adduser --uid $userid $username --disabled-password --gecos "$username"
	# else
	# 	adduser --uid $userid $username --disabled-password --no-create-home --gecos "$username"
	# fi

    # adduser: Don't create home directory.
	echo "Confirmation d'utilisateur du système..."
	lxc exec "$NAME" -- adduser --uid $userid $username --disabled-password --no-create-home --gecos "$username"

	# create user (smb account): (no need to delete); give default pwd if none set
	echo "Confirmation d'utilisateur de Samba..."
    pwd_status=$(lxc exec "$NAME" -- passwd --status $username | awk '{print $2}')
    if [[ $pwd_status == "NP" ]]; then
        echo "$username:$passwd" | lxc exec "$NAME" -- chpasswd
    fi

    smb_exists=$(lxc exec "$NAME" -- pdbedit -u $username || true)
    if [[ ! "$smb_exists" ]]; then
		echo "addition d'utilisateur Samba : $username"
        (echo $passwd; echo $passwd) | lxc exec "$NAME" -- smbpasswd -a -s $username
    fi
}

smbBlock() {
    folder="${1}"
    comment="${2}"
    path="${3}"
    browseable="${4}"
    mode="${5}"
    read_only="${6}"
    guest="${7}"
	lxc exec "$NAME" -- echo "
[$folder]
Comment = $comment
path = $path
browseable = $browseable
read only = "$read_only"
guest ok = $guest
create mask = 0$mode" | lxc exec "$NAME" -- tee -a /etc/samba/smb.conf
	echo "- Dossier << $folder >> ajouté à smb.conf."
}

# ------------------------------------------------------------------------------
# Create users & add to relevant non-user groups.
# ------------------------------------------------------------------------------
echo
echo
echo "------------------------------------"
echo "Addition de nouveaux groupes et utilisateurs..."

newGroup "2001" "$ADMIN_USER"
newUser "2001" "$ADMIN_USER" "Esaie53:7"
lxc exec "$NAME" -- adduser "$ADMIN_USER" "$ADMIN_USER"

# ------------------------------------------------------------------------------
# Create first section of /etc/samba/smb.conf.
# ------------------------------------------------------------------------------
# Determine ethernet interface.
#eth=$(nmcli device status | grep ethernet | awk '{print $1}')
eth=$(lxc exec "$NAME" -- ip -br addr | grep -Eow '^en[ps0-9]+' | head -n1)

echo
echo
echo "------------------------------------"
echo "Addition de nouveaux dossiers..."
echo
if lxc exec "$NAME" -- [[ ! -e /etc/samba/smb.conf.orig ]]; then
    lxc exec "$NAME" -- cp /etc/samba/smb.conf{,.orig}
    #echo "Backed up original smb.conf to smb.conf.orig and created new version."
    echo "Le fichier smb.conf a été sauvegardé."
fi

# Write [global] section, which does not change, to top of smb.conf.
echo "[global]
	workgroup = ACATBA
	server string = %h server (Samba, Ubuntu)
	interfaces = $eth
	server role = standalone server
	security = USER
	map to guest = bad user
	obey pam restrictions = yes
	pam password change = yes
	passwd program = /usr/bin/passwd %u
	passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
	unix password sync = yes
	log file = /var/log/samba/log.%m
	max log size = 1000
	os level = 65
	domain master = no
	dns proxy = no
	usershare allow guests = yes
	panic action = /usr/share/samba/panic-action %d
	idmap config * : backend = tdb

	# Added preferences by Nate
    log level = 3
    hide dot files = yes
    hide files = /~\$*/
    passdb backend = tdbsam" | lxc exec "$NAME" -- tee /etc/samba/smb.conf


# ------------------------------------------------------------------------------
# Créer le dossier de racine du partage.
# ------------------------------------------------------------------------------
folder="share"
mode="774"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Dossier de tous les utilisateurs"
path="$SHARE_BASE/$folder"
browseable="no"
read_only="no"
guest="no"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

# ------------------------------------------------------------------------------
# Créer des dossiers partagés.
# ------------------------------------------------------------------------------
folder="apps-etc"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Applications et ressources"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="formations"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Materiels de formation"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="guérison des traumatismes"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Ressources sur la guérison des traumatismes"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="linguistique"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Ressources sur la linguistique"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="transferts"
mode="777"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Pour transferer des fichiers au serveur"
path="$SHARE_BASE/$folder"
browseable="no"
read_only="no"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

folder="windows"
mode="775"
newFolder "${folder}" "$ADMIN_USER" "$ADMIN_USER" "${mode}"
comment="Mises a jour pour Windows"
path="$SHARE_BASE/$folder"
browseable="yes"
read_only="yes"
guest="yes"
smbBlock "$folder" "$comment" "$path" "$browseable" "$mode" "$read_only" "$guest"

# ------------------------------------------------------------------------------
# system-config-samba bug fix.
# ------------------------------------------------------------------------------
# /etc/libuser.conf needs to exist for system-config-samba to be able to launch
# when installing system-config-samba this file isn't created :-(
# touch /etc/libuser.conf

# ------------------------------------------------------------------------------
# Redémarrer Samba pour effectuer les changements.
# ------------------------------------------------------------------------------
lxc exec "$NAME" -- systemctl restart nmbd.service
lxc exec "$NAME" -- systemctl restart smbd.service

exit 0
