#!/bin/bash

# ==============================================================================
# - This script is used to install and configure backup capability:
#   - syncthing: synchronizing user files to the server
#   - crashplan: backing up server files to the cloud
#
# The general script outline is:
#	- Ensure dependencies are met
#	- Create (or modify) folders
#	- Create (or modify) smb.conf to match
#
# ==============================================================================
SCRIPT="${0}"
PARENT=$(dirname "${0}")


# ------------------------------------------------------------------------------
# Ensure elevated privileges.
# ------------------------------------------------------------------------------
if [[ $(id -u) != 0 ]]; then
    sudo "${0}"
    exit $?
fi

# ------------------------------------------------------------------------------
# Install & configure syncthing.
# ------------------------------------------------------------------------------
sthome="/etc/syncthing"
stuser="syncthing"
stcert="${sthome}/cert.pem"
stconfig="${sthome}/config.xml"
stsvc="syncthing@.service"
stsvc_file="/etc/systemd/system/${stsvc}"
stsrc="${PARENT}/data/syncthing"

# Verify installation.
if [[ ! $(which syncthing) ]]; then
    apt-get update
    apt-get --yes install syncthing
    ret=$?
    if [[ $ret != 0 ]]; then
        echo "Installation of syncthing failed. Please fix it and try again."
        exit 1
    fi
fi

# Verify syncthing user.
adduser --no-create-home --gecos 'syncthing,,,' --shell '/usr/sbin/nologin' --uid 2000 $stuser
#adduser $stuser acatbadmin # not sure if this is necessary

# Verify syncthing home folder.
mkdir -p "${sthome}"

# Verify config files.
if [[ ! -e $stconfig ]]; then
    cp "${stsrc}/config.xml" "$stconfig"
elif [[ ! -e $stcert ]]; then
    cp "${stsrc}/cert.pem" "${stcert}"
elif [[ ! -e $stsvc_file ]]; then
    cp "${stsrc}/${stsvc}" "${stsvc_file}"
    systemctl daemon-reload
fi

# Verify inotify config.
# Ref: https://docs.syncthing.net/users/faq.html#inotify-limits
config='fs.inotify.max_user_watches=204800'
if [[ ! $(grep "$config" /etc/sysctl.conf) ]]; then
    echo "$config" | tee -a /etc/sysctl.conf
fi

# Verify private key file.
keyfile="${sthome}/key.pem"
if [[ ! -e "$keyfile" ]]; then
    key=''
    if [[ -e ./key.pem ]]; then
        key=$(cat ./key.pem)
    else
        read -p "Please enter the \"key.pem\" for syncthing: " key
    fi
    if [[ ! "$key" ]]; then
        echo "The \"key.pem\" for syncthing is empty. Try again."
        exit 1
    else
        echo "$key" > "$keyfile"
    fi
fi

# Verify service status.
systemctl status --no-pager "syncthing@${stuser}.service"
stsvc_state=$?
if [[ ! $stsvc_state -ne 0 ]]; then
    systemctl enable --now "syncthing@${stuser}.service"
fi
